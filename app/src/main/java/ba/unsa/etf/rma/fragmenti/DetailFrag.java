package ba.unsa.etf.rma.fragmenti;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.GridViewAdapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFrag extends Fragment {

    private GridView gridKvizovi;
    public static GridViewAdapter gridViewAdapter;
    private ArrayList<Kviz> listaKvizova = new ArrayList<>();
    private Kategorija svi = new Kategorija("Svi","88");
    private ArrayList<Pitanje> pitanja = new ArrayList<Pitanje>();
    private Kviz kvizDodaj = new Kviz("Dodaj kviz", pitanja, svi);
    private ArrayList<Kviz> filtriraniNIz = new ArrayList<>();

    public DetailFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        gridKvizovi = (GridView) view.findViewById(R.id.gridKvizovi);
        listaKvizova = getArguments().getParcelableArrayList("listaKvizoveGrid");
        if(listaKvizova.size() == 0){
            listaKvizova.add(kvizDodaj);
        }

        Resources res = getResources();
        gridViewAdapter = new GridViewAdapter(getActivity(), listaKvizova, res);
        gridKvizovi.setAdapter(gridViewAdapter);


        gridKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long id) {
                if (listaKvizova.size() == 0) {
                    if (position != listaKvizova.size() - 1) {
                        KvizoviAkt.pozicijaKvizaKojegUredujemo = position;
                        KvizoviAkt.uredivanjeKviza = true;
                        Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                        myIntent.putExtra("naziv", listaKvizova.get(position).getNaziv());
                        myIntent.putExtra("listaPitanja", listaKvizova.get(position).getPitanja());
                        myIntent.putExtra("kategorija", listaKvizova.get(position).getKategorija().getNaziv());
                        startActivity(myIntent);
                        getActivity().startActivityForResult(myIntent, 98);
                    } else if (position == listaKvizova.size() - 1) {
                        Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                        getActivity().startActivityForResult(myIntent, 98);
                    }
                } else {
                    if (position != listaKvizova.size() - 1) {
                        KvizoviAkt.pozicijaKvizaKojegUredujemo = position;
                        KvizoviAkt.uredivanjeKviza = true;
                        Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                        myIntent.putExtra("naziv", listaKvizova.get(position).getNaziv());
                        myIntent.putExtra("listaPitanja", listaKvizova.get(position).getPitanja());
                        myIntent.putExtra("kategorija", listaKvizova.get(position).getKategorija().getNaziv());
                        getActivity().startActivityForResult(myIntent, 98);
                    } else if (position == listaKvizova.size() - 1) {
                        Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                        getActivity().startActivityForResult(myIntent, 98);
                    }
                }

                return true;
            }
        });
        gridKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position != listaKvizova.size() - 1) {
                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(getActivity(), IgrajKvizAkt.class);
                    intent.putExtra("naziv", listaKvizova.get(position).getNaziv());
                    intent.putExtra("listaPitanja", listaKvizova.get(position).getPitanja());
                    intent.putExtra("kategorija", listaKvizova.get(position).getKategorija().getNaziv());
                    intent.putExtra("idKategorije", listaKvizova.get(position).getKategorija().getId());
                   getActivity().startActivityForResult(intent, 98);

                }

            }
        });

        return view;
    }

}
