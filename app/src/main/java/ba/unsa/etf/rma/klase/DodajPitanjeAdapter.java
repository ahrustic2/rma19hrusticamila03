package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajPitanjeAkt;

public class DodajPitanjeAdapter extends BaseAdapter implements View.OnClickListener{

    private Activity aktivnost;
    private ArrayList lista;
    private static LayoutInflater inflater = null;
    public Resources res;
    Pitanje pitanje = null;
    int brojac = 0;

    public DodajPitanjeAdapter(Activity kvizoviAkt, ArrayList<Pitanje> kategorije, Resources resources) {
        aktivnost = kvizoviAkt;
        lista = kategorije;
        res = resources;
        inflater = ( LayoutInflater )aktivnost.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public int getCount() {
        if(lista.size()<=0)
            return 1;
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        public TextView naziv;
        public ImageView icon;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        DodajPitanjeAdapter.ViewHolder holder;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.element, null);
            holder = new DodajPitanjeAdapter.ViewHolder();
            holder.naziv = (TextView) vi.findViewById(R.id.etNaziv);
            holder.icon = (ImageView) vi.findViewById(R.id.icon);
            TacanOdgovor item = new TacanOdgovor(getItem(position).toString());
            if(DodajPitanjeAkt.tacanOdgovor == item) {
                convertView.setBackgroundColor(Color.GREEN);
            }

            vi.setTag(holder);
        } else {
            holder = (DodajPitanjeAdapter.ViewHolder) vi.getTag();
        }
        if (lista.size() <= 0) {
            holder.naziv.setText("Lista mogucih pitanja je prazna");
            holder.icon.setImageResource(R.drawable.blue_icon);
        } else {
            pitanje = (Pitanje) lista.get(position);
            holder.naziv.setText(pitanje.getNaziv());
            holder.icon.setImageResource(pitanje.getImage());
        }
        return vi;
    }

}
