package ba.unsa.etf.rma.aktivnosti;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.support.annotation.IntegerRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.maltaisn.icondialog.IconDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.AsyncResponse;
import ba.unsa.etf.rma.klase.GridViewAdapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.DodajKategorijaAdapter;
import ba.unsa.etf.rma.klase.DodajKvizAdapter;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.SQLBazaHelper;

import static com.google.api.client.googleapis.auth.oauth2.GoogleCredential.fromStream;
import static java.lang.Integer.compare;
import static java.lang.Integer.parseInt;

public class KvizoviAkt extends AppCompatActivity implements AdapterView.OnItemSelectedListener, ListaFrag.OnListaFrag{

    private static final int MY_CAL_REQ = 0;
    public static ArrayList<Kategorija> kategorije = new ArrayList<Kategorija>();
    private ListView lvKvizovi;
    private Spinner spPostojeceKategorije;
    public static ArrayList<Kviz> kvizovi = new ArrayList<>();
    public static ArrayList<Kviz> novaListaKvizova = new ArrayList<Kviz>();
    private Kategorija svi = new Kategorija("Svi","88");
    private ArrayList<Pitanje> pitanja = new ArrayList<Pitanje>();
    private DodajKvizAdapter adapter;
    private DodajKategorijaAdapter adapter2;
    public static ArrayList<String> pomocni = new ArrayList<>();
    public static boolean uredivanjeKviza = false;
    public static int pozicijaKvizaKojegUredujemo ;
    public static int pozicijaKategorije;
    boolean sirinaL = false;
    boolean sirokiEkran = false;
    private GridView gridKvizovi;
    public static ArrayList<Kviz> filtriraniGridKvizovi = new ArrayList<>();
    private Kviz kvizDodaj = new Kviz("Dodaj kviz", pitanja, svi);
    private FrameLayout listPlace;
    private FrameLayout detailPlace;
    private ListaFrag listaFrag;
    private DetailFrag detailFrag;
    ArrayList<Kviz> filtriranaLista = new ArrayList<>();
    //BAZA
    public static Map<Kategorija, String> listaKategorijaIzBaze = new HashMap<>();
    public static Map<Kviz, String> kvizoviIzBaze = new HashMap<>();
    public static Map<Pitanje, String> pitanjaIzBaze = new HashMap<>();
    public static String idKvizaZaUredivanje;
    private String idKategorijeZaFiltriranje;
    public  ArrayList<Kviz> ucitaniKvizovi = new ArrayList<>();
    public  Map<Kviz, String> ucitaniKB = new HashMap<>();
    private Map<Integer, Map<String, String>> listaIzBaze = new HashMap<>();
    private ArrayList<Pair <Map<Integer, Map<String, String>>, String>> rangListaIzFirebase = new ArrayList<>();
    public static ArrayList<Pair <Map<Integer, Map<String, String>>, String>> rangListaIzSQL = new ArrayList<>();

    //KONEKCIJA
    public static boolean konekcija;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean status = daLiJeKonektovano(KvizoviAkt.this);
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
               konekcija = status;
            }
            if (konekcija) {
                Toast.makeText(getApplicationContext(), "Spojeni ste na internet!", Toast.LENGTH_LONG).show();
            }
            else if (!konekcija){
                Toast.makeText(getApplicationContext(), "Niste spojeni na internet!", Toast.LENGTH_LONG).show();
            }
        }
    };

    //KALENDAR
    private Cursor cursor;
    private boolean imaDopustenje = true;
    private ContentResolver contentResolver;
    public Map<String, String> podaciIzKalendara = new HashMap<>();
    //LOKALNA BAZA
    public static SQLiteDatabase database = null;
    public static SQLBazaHelper sqlBazaHelper;
    public static ArrayList<Pitanje> pitanjaIzSQL = new ArrayList<>();

    public Map<String, String> ucitajKalendar(){
        cursor = null;
        contentResolver = getContentResolver();
        String[] vrijednosti = {
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.TITLE
        };
        Uri uri = CalendarContract.Events.CONTENT_URI;
        cursor = getContentResolver().query(uri, vrijednosti, null, null, null);
        while (cursor.moveToNext()){
                String pocetakDogadaja = cursor.getString(0);
                String nazivDogadaja = cursor.getString(1);
                podaciIzKalendara.put(nazivDogadaja, pocetakDogadaja);
        }
        cursor.close();
        return podaciIzKalendara;
    }


    public void ucitajUSQLBazu (){
        sqlBazaHelper.obrisiSQLBazu();
        for (int i = 0; i < kategorije.size(); i++){
            sqlBazaHelper.dodajKategorijuUSQL(kategorije.get(i), database);
        }
        Iterator iter = pitanjaIzBaze.entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry par = (Map.Entry) iter.next();
            Pitanje pitanje = (Pitanje) par.getKey();
            sqlBazaHelper.dodajPitanjeUSQL(pitanje, database);
        }
        Iterator iterator = kvizoviIzBaze.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry par = (Map.Entry) iterator.next();
            Kviz kviz = (Kviz) par.getKey();
            sqlBazaHelper.dodajKvizUSQL(kviz, database);
        }
    }


    public boolean provjeraDogadaja(int position){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date trenutnoVrijeme = new Date();
        Map<String, String> mapa = ucitajKalendar();

        Iterator iterator = podaciIzKalendara.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry par = (Map.Entry) iterator.next();
            if (par.getValue() == "" || par.getValue() == "") continue;
            else {
            long vrijeme = Long.parseLong(String.valueOf((par.getValue())));
            Date datum = new Date(vrijeme);
            if(simpleDateFormat.format(trenutnoVrijeme).equals(simpleDateFormat.format(datum))){
                int duzinaKviza = (int)Math.round(kvizovi.get(position).getPitanja().size()/2.);
                long trenutnoUMilisekundama = trenutnoVrijeme.getTime();
                long eventUMilisekundama = datum.getTime();
                long duzinaKvizaUMilisekundama = TimeUnit.MINUTES.toMillis(duzinaKviza);
                int minuteDoEventa = (int) (TimeUnit.MILLISECONDS.toMinutes(eventUMilisekundama - trenutnoUMilisekundama) + 1);

                System.out.println();

                if(trenutnoUMilisekundama + duzinaKvizaUMilisekundama > eventUMilisekundama && trenutnoUMilisekundama < eventUMilisekundama){
                    new AlertDialog.Builder(KvizoviAkt.this)
                            .setTitle("UPOZORENJE")
                            .setMessage("Imate događaj u kalendaru za " + minuteDoEventa + " minuta!")
                            .setNegativeButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return true;
                }
            }
            }
        }
        return false;
    }

    public void pomocnaOnCreate(){
        setContentView(R.layout.activity_kvizovi_akt);
        Resources resources = getResources();
        lvKvizovi = (ListView) findViewById(R.id.lvKvizovi);
        if (lvKvizovi == null) sirokiEkran = true;
        if (sirokiEkran) {
            FragmentManager fm = getSupportFragmentManager();
            FrameLayout listPlace = (FrameLayout) findViewById(R.id.listPlace);
            FrameLayout detailPlace = (FrameLayout) findViewById(R.id.detailPlace);

            if (detailPlace != null) {
                sirinaL = true;
                DetailFrag detailFrag;
                ListaFrag listaFrag;
                detailFrag = (DetailFrag) fm.findFragmentById(R.id.detailPlace);
                listaFrag = (ListaFrag) fm.findFragmentById(R.id.listPlace);

                if (detailFrag == null) {
                    detailFrag = new DetailFrag();
                    ArrayList<Kviz> kvizoviZaGrid = novaListaKvizova;
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("listaKvizoveGrid", kvizoviZaGrid);
                    detailFrag.setArguments(bundle);
                    fm.beginTransaction().replace(R.id.detailPlace, detailFrag).commit();

                }

                if (listaFrag == null) {
                    listaFrag = new ListaFrag();
                    ArrayList<Kategorija> kategorijeZaGrid = kategorije;
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("sveKategorijeZaGrid", kategorijeZaGrid);
                    listaFrag.setArguments(bundle);
                    fm.beginTransaction().replace(R.id.listPlace, listaFrag).commit();
                } else {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }

        } else {

            spPostojeceKategorije = (Spinner) findViewById(R.id.spPostojeceKategorije);
            spPostojeceKategorije.setAdapter(adapter2);
            spPostojeceKategorije.setFocusable(true);
            spPostojeceKategorije.setFocusableInTouchMode(true);

            Resources res = getResources();
            adapter = new DodajKvizAdapter(this, kvizovi, res);
            adapter2 = new DodajKategorijaAdapter(this, kategorije, res);

            adapter.notifyDataSetChanged();
            adapter2.notifyDataSetChanged();
            lvKvizovi.setAdapter(adapter);
            spPostojeceKategorije.setAdapter(adapter2);
        }
        lvKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position != novaListaKvizova.size() - 1) {
                    boolean daLiImaDogadaj = false;
                    daLiImaDogadaj = provjeraDogadaja(position);
                    if (!daLiImaDogadaj) {
                        Bundle bundle = new Bundle();
                        Intent intent = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                        intent.putExtra("naziv", novaListaKvizova.get(position).getNaziv());
                        intent.putExtra("listaPitanja", novaListaKvizova.get(position).getPitanja());
                        intent.putExtra("kategorija", novaListaKvizova.get(position).getKategorija().getNaziv());
                        intent.putExtra("idKategorije", novaListaKvizova.get(position).getKategorija().getId());
                        KvizoviAkt.this.startActivityForResult(intent, 22);
                    }
                }
            }
        });
    }


    public boolean daLiJeKonektovano(Context context) {

        boolean wifi = false;
        boolean mobilniPodaci = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netinfo = connectivityManager.getAllNetworkInfo();

        for (NetworkInfo info : netinfo) {
            if (info.getTypeName().equalsIgnoreCase("WIFI"))
                if (info.isConnected())
                    wifi = true;

            if (info.getTypeName().equalsIgnoreCase("MOBILE"))
                if (info.isConnected())
                    mobilniPodaci = true;
        }
        return wifi||mobilniPodaci;
    }




    public class Baza extends AsyncTask<String, Void, String>{

        AsyncResponse delegat = null;
        public Baza(AsyncResponse delegat){this.delegat = delegat;}

        @Override
        protected void onPostExecute(String res) {
            delegat.zavrsenProces(res);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... strings) {
            InputStream stream = getResources().openRawResource(R.raw.secret);

            try {
                GoogleCredential credential = GoogleCredential.fromStream(stream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credential.refreshToken();
                String TOKEN = credential.getAccessToken();
                Log.d("Sada je token: ", TOKEN);

                //UCITAVANJE KATEGORIJA IZ BAZE
                String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connectionKat  = (HttpURLConnection) urlObj.openConnection();
                connectionKat.connect();
                InputStream odgovorKat = connectionKat.getInputStream();
                StringBuilder responseKat = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovorKat, "utf-8"))
                ){
                    responseKat = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        responseKat.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR KATEGORIJA: ", responseKat.toString());
                }

                String rezultatKat = responseKat.toString();
                if(!rezultatKat.equals("{}")) {
                    JSONObject jsonObject = new JSONObject(rezultatKat);
                    JSONArray dokument = jsonObject.getJSONArray("documents");
                    for (int i = 0; i < dokument.length(); i++) {
                        JSONObject documents = dokument.getJSONObject(i);
                        String name = documents.getString("name");

                        int k;
                        boolean imaCrticu = false;
                        for (k = name.length() - 1; k > 0; k--) {
                            if (name.charAt(k) == '/'){
                                imaCrticu = true;
                                break;
                            }
                        }
                        if (imaCrticu) k++;
                        String idK = name.substring(k, name.length());
                        JSONObject fields = documents.getJSONObject("fields");
                        JSONObject value = fields.getJSONObject("naziv");
                        String nazivKat = value.getString("stringValue");
                        if (nazivKat.equals("Svi")) continue;
                        if (nazivKat.equals("Dodaj kategoriju")) continue;
                        JSONObject vrijednostIkone = fields.getJSONObject("idIkonice");
                        int idIkonice = vrijednostIkone.getInt(("integerValue"));

                        Kategorija kat = new Kategorija(nazivKat, String.valueOf(idIkonice));
                        listaKategorijaIzBaze.put(kat, idK);
                        kategorije.add(kat);
                    }
                }

                //UCITAVANJE PITANJA IZ BAZE
                String urlPitanje = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Pitanja?access_token=";
                URL urlObjPitanje = new URL (urlPitanje + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connectionP  = (HttpURLConnection) urlObjPitanje.openConnection();
                connectionP.connect();
                InputStream odgovorP = connectionP.getInputStream();
                StringBuilder responseP = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovorP, "utf-8"))
                ){
                    responseP = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        responseP.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR PITANJE: ", responseP.toString());
                }
                String rezultatP = responseP.toString();
                JSONObject jsonObjectP = new JSONObject(rezultatP);
                JSONArray documentsP = jsonObjectP.getJSONArray("documents");
                for(int i = 0; i < documentsP.length(); i++) {
                    JSONObject doc = documentsP.getJSONObject(i);
                    String nameP = doc.getString("name");
                    JSONObject fieldsP = doc.getJSONObject("fields");
                    JSONObject stringValue = fieldsP.getJSONObject("naziv");
                    String nazivP = stringValue.getString("stringValue");
                    if(nazivP.equals("Dodaj pitanje")) continue;

                    JSONObject intVrijednostZaTacan = fieldsP.getJSONObject("indexTacnog");
                    int indexTacnog = intVrijednostZaTacan.getInt("integerValue");

                    JSONObject odgovoriJO = fieldsP.getJSONObject("odgovori");
                    JSONObject listaVrijednost = odgovoriJO.getJSONObject("arrayValue");
                    JSONArray lista = listaVrijednost.getJSONArray("values");
                    ArrayList<String> odgovoriZaTrenutnoPitanje = new ArrayList<>();
                    for (int j = 0; j < lista.length(); j++) {
                        JSONObject element = lista.getJSONObject(j);
                        String odg = element.getString("stringValue");
                        odgovoriZaTrenutnoPitanje.add(odg);
                    }

                    String trenutniTacanOdgovor = "";
                    for (int j = 0; j < odgovoriZaTrenutnoPitanje.size(); j++) {
                        if (j == indexTacnog) {
                            trenutniTacanOdgovor = odgovoriZaTrenutnoPitanje.get(j);
                            break;
                        }
                    }
                    Pitanje pitanje = new Pitanje(nazivP, nazivP, odgovoriZaTrenutnoPitanje, trenutniTacanOdgovor);
                    int k;
                    boolean imaCrticu = false;
                    for(k = nameP.length() - 1; k > 0; k--) {
                        if (nameP.charAt(k) == '/'){
                           imaCrticu = true;
                            break;
                        }
                    }
                    if (imaCrticu) k++;
                    String idPit = nameP.substring(k, nameP.length());
                    pitanjaIzBaze.put(pitanje,nameP);
                }


                //UCITAVANJE KVIZOVA IZ BAZE
                String urlKviz = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObjKviz = new URL (urlKviz + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connectionKviz = (HttpURLConnection) urlObjKviz.openConnection();
                connectionKviz.connect();
                InputStream odgovorKviz = connectionKviz.getInputStream();
                StringBuilder responseKviz = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovorKviz, "utf-8"))
                ){
                    responseKviz = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        responseKviz.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR KVIZOVI: ", responseKviz.toString());
                }
                String rezultatKvizovi = responseKviz.toString();
                JSONObject joKvizovi = new JSONObject(rezultatKvizovi);
                JSONArray documentsKvizovi = joKvizovi.getJSONArray("documents");
                for(int i = 0; i < documentsKvizovi.length(); i++) {
                    JSONObject doc = documentsKvizovi.getJSONObject(i);
                    String nameKviza = doc.getString("name");
                    JSONObject fields = doc.getJSONObject("fields");
                    JSONObject stringVrijednost = fields.getJSONObject("naziv");
                    String nazivKviza = stringVrijednost.getString("stringValue");

                    if(nazivKviza.equals("Dodaj kviz")) continue;
                    JSONObject referenceValue = fields.getJSONObject("idKategorije");
                    String idKategorije = referenceValue.getString("stringValue");
                    Kategorija ka = new Kategorija();
                    Iterator iterator = listaKategorijaIzBaze.entrySet().iterator();
                    while(iterator.hasNext())
                    {
                        Map.Entry pair = (Map.Entry) iterator.next();
                        Kategorija kaqt = (Kategorija) pair.getKey();
                        String sifra = (String) pair.getValue();
                        int s;
                        boolean imaCrticu = false;
                        for(s = sifra.length() - 1; s > 0; s--) {
                            if (sifra.charAt(s) == '/'){
                                imaCrticu = true;
                                break;
                            }
                        }

                        if (imaCrticu) s++;
                        String upor = sifra.substring(s, sifra.length());
                        if (idKategorije.equals(upor)){
                                ka = kaqt;
                                break;
                        }
                    }


                    JSONObject p = fields.getJSONObject("pitanja");
                    JSONObject arrayValue = p.getJSONObject("arrayValue");
                    if (arrayValue.has("values")) {
                        JSONArray values = arrayValue.getJSONArray("values");
                        ArrayList<String> idPitanja = new ArrayList<>();
                        for (int j = 0; j < values.length(); j++) {
                            JSONObject item = values.getJSONObject(j);
                            String pit = item.getString("stringValue");
                            idPitanja.add(pit);
                        }
                        ArrayList<Pitanje> pitanjaZaKviz = new ArrayList<>();

                        Iterator it = pitanjaIzBaze.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry) it.next();
                            Pitanje pita = (Pitanje) pair.getKey();
                            String sifra = (String) pair.getValue();

                            int k;

                            boolean imaCrticu = false;
                            for (k = sifra.length() - 1; k > 0; k--) {
                                if (sifra.charAt(k) == '/'){
                                    imaCrticu = true;
                                    break;
                                }
                            }
                            if (imaCrticu) k++;
                            sifra = sifra.substring(k, sifra.length());
                            for (int h = 0; h < idPitanja.size(); h++) {
                                if (idPitanja.get(h).equals(sifra)) {

                                    pitanjaZaKviz.add((Pitanje) pair.getKey());
                                }
                            }
                        }

                        idPitanja = new ArrayList<>();
                        Kviz kvizIzBaze = new Kviz(nazivKviza, pitanjaZaKviz, ka);
                        kvizoviIzBaze.put(kvizIzBaze, nameKviza);
                        if (novaListaKvizova.size() != 0) {
                            novaListaKvizova.add(novaListaKvizova.size() - 1, kvizIzBaze);
                        } else novaListaKvizova.add(novaListaKvizova.size(), kvizIzBaze);
                        if (kvizovi.size() != 0) {
                            kvizovi.add(kvizovi.size() - 1, kvizIzBaze);
                        } else {
                            kvizovi.add(kvizovi.size(), kvizIzBaze);
                        }
                    }
                    else {
                        ArrayList<Pitanje> pZaKviz = new ArrayList<>();
                        Kviz kvizIzBaze = new Kviz (nazivKviza, pZaKviz, ka);
                        kvizoviIzBaze.put(kvizIzBaze, nameKviza);
                        if (novaListaKvizova.size() != 0) {
                            novaListaKvizova.add(novaListaKvizova.size() - 1, kvizIzBaze);
                        } else novaListaKvizova.add(novaListaKvizova.size(), kvizIzBaze);
                        if (kvizovi.size() != 0) {
                            kvizovi.add(kvizovi.size() - 1, kvizIzBaze);
                        } else {
                            kvizovi.add(kvizovi.size(), kvizIzBaze);
                        }
                    }
                }

                //UCITAVANJE RANG LISTE
                String urlRangLista = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Rangliste?access_token=";
                URL urlObjRangLista = new URL (urlRangLista + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connectionRangLista = (HttpURLConnection) urlObjRangLista.openConnection();
                connectionRangLista.connect();
                InputStream odgovorRangLista = connectionRangLista.getInputStream();
                StringBuilder responseRangLista = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovorRangLista, "utf-8"))
                ){
                    responseRangLista = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        responseRangLista.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR RANG LISTE: ", responseRangLista.toString());
                }
                String rezultat = responseRangLista.toString();
                if(!rezultat.equals("{}")) {
                    JSONObject jsonObject = new JSONObject(rezultat);
                    JSONArray documents = jsonObject.getJSONArray("documents");
                    for (int i = 0; i < documents.length(); i++) {
                        JSONObject doc = documents.getJSONObject(i);
                        JSONObject fields = doc.getJSONObject("fields");
                        JSONObject value = fields.getJSONObject("lista");
                        JSONObject value2 = value.getJSONObject("mapValue");
                        JSONObject fieldsMape = value2.getJSONObject("fields");
                        JSONObject mapa = fieldsMape.getJSONObject("mapa");
                        JSONObject mapa2 = mapa.getJSONObject("mapValue");
                        JSONObject poljaDrugo = mapa2.getJSONObject("fields");
                        JSONObject vrijednostProcenta = poljaDrugo.getJSONObject("procenat");
                        String procenat = vrijednostProcenta.getString("stringValue");
                        JSONObject vrijednostImenaIgraca = poljaDrugo.getJSONObject("imeIgraca");
                        String imeIgraca = vrijednostImenaIgraca.getString("stringValue");
                        JSONObject integerValue = fieldsMape.getJSONObject("pozicija");
                        int pozicijaIgraca = integerValue.getInt("integerValue");
                        JSONObject stringValue = fields.getJSONObject("nazivKviza");
                        String nazivKvizaIzRanga = stringValue.getString("stringValue");
                        Map<String, String> drugaMapa = new HashMap<>();
                        drugaMapa.put(imeIgraca, procenat);
                        listaIzBaze.put(pozicijaIgraca, drugaMapa);
                        rangListaIzFirebase.add(new Pair<Map<Integer, Map<String, String>>, String>(listaIzBaze, nazivKvizaIzRanga));
                        double procenaDouble = Double.parseDouble(procenat);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        public String convertStreamToString(InputStream is){
            BufferedReader reader = new BufferedReader(new InputStreamReader((is)));
            StringBuilder sb = new StringBuilder();
            String line = null;
            try
            {
              while ((line = reader.readLine()) != null) sb.append(line + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }

    }

    public class DodajUFirebase extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                System.out.println("SADA JE TOKEN: " + TOKEN);
                for (int i = rangListaIzFirebase.size(); i < rangListaIzSQL.size(); i++) {
                    String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Rangliste?access_token=";
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                    HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("Accept", "application/json");


                    String dokument = "{\"fields\" : { \"nazivKviza\" : { \"stringValue\" : \"" + rangListaIzSQL.get(i).second + "\" }, \"lista\" : { \"mapValue\" : { \"fields\" : { \"pozicija\" : { \"integerValue\" : \"";
                    Iterator it = rangListaIzSQL.get(i).first.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry m = (Map.Entry) it.next();
                        int pozicija = (int) m.getKey();
                        dokument += String.valueOf(pozicija);
                        dokument += "\" }, \"mapa\" : { \"mapValue\" : { \"fields\" : { \"procenat\" : { \"stringValue\" : \"";
                        Map<String, String> unutrasnja = (Map<String, String>) m.getValue();
                        Iterator it2 = unutrasnja.entrySet().iterator();
                        while (it2.hasNext()) {
                            Map.Entry m2 = (Map.Entry) it2.next();
                            String igrac = (String) m2.getKey();
                            String proc = (String) m2.getValue();
                            dokument += proc + "\" }, \"imeIgraca\" : { \"stringValue\" : \"" + igrac + "\" }}}}}}}}}";
                        }
                    }
                    try (OutputStream outputStream = connection.getOutputStream()) {
                        byte[] input = dokument.getBytes("utf-8");
                        outputStream.write(input, 0, input.length);
                    }

                    int code = connection.getResponseCode();
                    InputStream odgovor = connection.getInputStream();
                    StringBuilder response = null;
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(odgovor, "utf-8"))
                    ) {
                        response = new StringBuilder();
                        String rensponseLine = null;
                        while ((rensponseLine = br.readLine()) != null) {
                            response.append(rensponseLine.trim());
                        }
                        Log.d("ODGOVOR", response.toString());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }




    //FILTRIRANJE
    public class FiltriranjeKvizova extends AsyncTask<String, Void, Void>{

        AsyncResponse ar = null;

        public FiltriranjeKvizova(AsyncResponse dodaj_kviz) {
            this.ar = dodaj_kviz;
        }

        @Override
        protected Void doInBackground(String... strings) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                System.out.println("SADA JE TOKEN: " + TOKEN);
                String query = "{\n" +
                        "\"structuredQuery\": {\n" +
                        "\"where\": {\n" +
                        "\"fieldFilter\": {\n" +
                        "\"field\": {\"fieldPath\": \"idKategorije\"}, \n" +
                        "\"op\": \"EQUAL\",\n" +
                        "\"value\": {\"stringValue\": \"" + idKategorijeZaFiltriranje + "\"}\n" +
                        "}\n" +
                        "},\n" +
                        "\"select\": {\"fields\": [ {\"fieldPath\": \"idKategorije\"}, {\"fieldPath\": \"naziv\"}, {\"fieldPath\": \"pitanja\"} ] }, \n" +
                        "\"from\": [{\"collectionId\" : \"Kvizovi\"}], \n" +
                        "\"limit\" : 2000\n" +
                        "}\n" +
                        "}";
                String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents:runQuery?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                boolean nadenaKategorija = false;
                try (OutputStream os = connection.getOutputStream()) {
                    byte[] input = query.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                StringBuilder response = null;
                InputStream odgovor = connection.getInputStream();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))
                ) {
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR: ", response.toString());
                }

                String rezultatKvizovi = response.toString();
                rezultatKvizovi = "{ \"documents\": " + rezultatKvizovi + "}";
                if(!rezultatKvizovi.equals("{}")) {
                    JSONObject joKvizovi = new JSONObject(rezultatKvizovi);
                    JSONArray documentsKvizovi = joKvizovi.getJSONArray("documents");
                    for (int i = 0; i < documentsKvizovi.length(); i++) {
                        JSONObject docuu = documentsKvizovi.getJSONObject(i);
                        if (docuu.has("document")) {
                            JSONObject doccii = docuu.getJSONObject("document");
                            String nameKviza = doccii.getString("name");
                            boolean imaCrtica = false;
                            int k;
                            for (k = nameKviza.length() - 1; k > 0; k--) {
                                if (nameKviza.charAt(k) == '/'){
                                    imaCrtica = true;
                                    break;
                                }
                            }
                            if (imaCrtica) k++;
                            nameKviza = nameKviza.substring(k, nameKviza.length());

                            JSONObject fields = doccii.getJSONObject("fields");
                            JSONObject stringValue = fields.getJSONObject("naziv");
                            String nazivKviza = stringValue.getString("stringValue");
                            if (nazivKviza.equals("Dodaj kviz")) continue;
                            JSONObject referenceValue = fields.getJSONObject("idKategorije");
                            String idKategorijeKviza = referenceValue.getString("stringValue");
                            Kategorija kat = new Kategorija();

                            Iterator iterator = listaKategorijaIzBaze.entrySet().iterator();

                            while(iterator.hasNext()){
                                Map.Entry par = (Map.Entry) iterator.next();
                                Kategorija pomocna =(Kategorija) par.getKey();
                                String sifra = (String) par.getValue();

                                if(idKategorijeKviza.equals(sifra)){
                                    kat = pomocna;
                                    nadenaKategorija = true;
                                    break;
                                }

                            }

                            JSONObject pittaa = fields.getJSONObject("pitanja");
                            JSONObject arrayValue = pittaa.getJSONObject("arrayValue");
                            if (arrayValue.has("values")) {
                                JSONArray values = arrayValue.getJSONArray("values");
                                ArrayList<String> ideviPitanja = new ArrayList<>();
                                for (int j = 0; j < values.length(); j++) {
                                    JSONObject item = values.getJSONObject(j);
                                    String pit = item.getString("stringValue");
                                    ideviPitanja.add(pit);
                                }
                                ArrayList<Pitanje> pitanjaZaKviz = new ArrayList<>();

                                Iterator it = pitanjaIzBaze.entrySet().iterator();
                                while (it.hasNext()){
                                    Map.Entry pair = (Map.Entry) it.next();
                                    Pitanje pitanje = (Pitanje) pair.getKey();
                                    String kod = (String) pair.getValue();
                                    for (int h = 0; h < ideviPitanja.size(); h++){
                                        if (ideviPitanja.get(h).equals(kod)){
                                            pitanjaZaKviz.add(pitanje);
                                        }
                                    }
                                }

                                ideviPitanja.clear();
                                Kviz trenutniKviz = new Kviz(nazivKviza, pitanjaZaKviz, kat);
                                novaListaKvizova.add(trenutniKviz);
                            } else {
                                ArrayList<Pitanje> pitanja = new ArrayList<>();
                                Kviz trenutniKviz = new Kviz(nazivKviza, pitanja, kat);
                                novaListaKvizova.add(trenutniKviz);
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ar.zavrsenProces("");
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        konekcija = daLiJeKonektovano(KvizoviAkt.this);
        rangListaIzSQL.clear();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiver, intentFilter);


        sqlBazaHelper = new SQLBazaHelper(this);
        try {
            database = sqlBazaHelper.getWritableDatabase();
        } catch (SQLException e) {
            database = sqlBazaHelper.getReadableDatabase();
        }


        rangListaIzSQL = sqlBazaHelper.rangListaIzSQL();


        //Citanje iz kalendara
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            imaDopustenje = false;
        }

        if (!imaDopustenje) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALENDAR}, MY_CAL_REQ);
        }

        if (imaDopustenje) {
            ucitajKalendar();
        }

        if (!konekcija) {
            kategorije = sqlBazaHelper.katgorijeIzSQL(database);
            pitanjaIzSQL = sqlBazaHelper.pitanjaIZSQL(database);
            kvizovi = sqlBazaHelper.kvizoviIZSQL(database);
            kvizovi.add(kvizDodaj);
            novaListaKvizova = sqlBazaHelper.kvizoviIZSQL(database);
            novaListaKvizova.add(kvizDodaj);
            pomocnaOnCreate();
            spPostojeceKategorije.setAdapter(adapter2);
            Resources res = getResources();
            spPostojeceKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {

                public void onItemSelected(AdapterView<?> arg0,View arg1, int arg2, long arg3) {
                    novaListaKvizova = new ArrayList<Kviz>();
                    for(int i = 0; i < kvizovi.size(); i++) novaListaKvizova.add(kvizovi.get(i));
                    adapter = new DodajKvizAdapter(KvizoviAkt.this, kvizovi, res);
                    lvKvizovi.setAdapter(adapter);

                    int index = spPostojeceKategorije.getSelectedItemPosition();
                    Kategorija k = kategorije.get(index);
                    if(k.getNaziv().equals(svi.getNaziv())) {
                        novaListaKvizova = new ArrayList<Kviz>();
                        for(int i = 0; i < kvizovi.size(); i++) novaListaKvizova.add(kvizovi.get(i));
                        adapter = new DodajKvizAdapter(KvizoviAkt.this, kvizovi, res);
                        lvKvizovi.setAdapter(adapter);
                    }
                    else {
                        novaListaKvizova = new ArrayList<Kviz>();
                        for (int i = 0; i < kvizovi.size(); i++) {
                            if (kvizovi.get(i).getKategorija().getNaziv().equals(k.getNaziv())) {
                                novaListaKvizova.add(kvizovi.get(i));
                            }
                        }

                        novaListaKvizova.add(kvizDodaj);
                        adapter = new DodajKvizAdapter(KvizoviAkt.this, novaListaKvizova, res);
                        lvKvizovi.setAdapter(adapter);
                        lvKvizovi.deferNotifyDataSetChanged();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Resources res = getResources();
                    novaListaKvizova = new ArrayList<>();
                    for (int i = 0; i < kvizovi.size(); i++)
                        novaListaKvizova.add(kvizovi.get(i));
                    adapter = new DodajKvizAdapter(KvizoviAkt.this, kvizovi, res);
                    lvKvizovi.setAdapter(adapter);
                }
            });
        }

        if (kvizovi.size() == 0) {
            kvizovi.add(kvizDodaj);
        }
        if (kategorije.size() == 0) {
            kategorije.add(0, svi);
        }
        if (konekcija) {

            if (kvizoviIzBaze.size() == 0) {

                new Baza((output) -> {

                    kvizovi.clear();

                    boolean imaDodaj = false;
                    for (int i = 0; i < kvizovi.size(); i++) {
                        if (kvizovi.get(i).equals(kvizDodaj)) imaDodaj = true;
                    }
                    if (!imaDodaj) kvizovi.add(kvizDodaj);

                    Iterator iterator = kvizoviIzBaze.entrySet().iterator();
                    while (iterator.hasNext()) {
                        Map.Entry pair = (Map.Entry) iterator.next();
                        Kviz k = (Kviz) pair.getKey();
                        kvizovi.add(kvizovi.size() - 1, k);
                    }


                    ucitajUSQLBazu();

                    if (rangListaIzSQL.size() == 0){
                        rangListaIzSQL = rangListaIzFirebase;
                        for (int i = 0; i < rangListaIzFirebase.size(); i++){
                            String nazivKvizaBaza = rangListaIzFirebase.get(i).second;
                            Iterator ite = rangListaIzFirebase.get(i).first.entrySet().iterator();
                            while (ite.hasNext()){
                                Map.Entry par1 = (Map.Entry) ite.next();
                                int pozicijaBaza = (int) par1.getKey();
                                String pozicijaBaza2 = String.valueOf(pozicijaBaza);
                                Map<String, String> imeIprocenat = (Map<String, String>) par1.getValue();
                                Iterator it = imeIprocenat.entrySet().iterator();
                                while (it.hasNext()){
                                    Map.Entry par2 = (Map.Entry) it.next();
                                    String igrac = (String) par2.getKey();
                                    String procenat = (String) par2.getValue();
                                    sqlBazaHelper.dodajRanglistuUSQL(nazivKvizaBaza, pozicijaBaza2, igrac, procenat, database);

                                }
                            }

                        }
                    }
                    else if (rangListaIzSQL.size() > rangListaIzFirebase.size()){
                      new DodajUFirebase().execute();
                    }

                    pomocnaOnCreate();

                        spPostojeceKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                                novaListaKvizova.clear();
                                if (kategorije.get(spPostojeceKategorije.getSelectedItemPosition()).getNaziv().equals(svi.getNaziv())) {
                                    Resources res = getResources();
                                    for (int i = 0; i < kvizovi.size(); i++) {
                                        novaListaKvizova.add(kvizovi.get(i));
                                    }
                                    adapter = new DodajKvizAdapter(KvizoviAkt.this, kvizovi, res);
                                    lvKvizovi.setAdapter(adapter);
                                } else {
                                    String spinerKate = kategorije.get(spPostojeceKategorije.getSelectedItemPosition()).getNaziv();

                                    Iterator iterator1 = listaKategorijaIzBaze.entrySet().iterator();
                                    while (iterator1.hasNext()) {
                                        Map.Entry pair = (Map.Entry) iterator1.next();
                                        Kategorija kato = (Kategorija) pair.getKey();
                                        if (spinerKate.equals(kato.getNaziv())) {
                                            idKategorijeZaFiltriranje = (String) pair.getValue();
                                            break;
                                        }
                                    }


                                    new FiltriranjeKvizova((output) -> {
                                        boolean imaDodaj = false;

                                        for (int i = 0; i < novaListaKvizova.size(); i++) {
                                            if (novaListaKvizova.get(i).getNaziv().equals(kvizDodaj.getNaziv()))
                                                imaDodaj = true;
                                        }
                                        if (!imaDodaj) {
                                            novaListaKvizova.add(kvizDodaj);
                                        }
                                        Resources res = getResources();
                                        adapter = new DodajKvizAdapter(KvizoviAkt.this, novaListaKvizova, res);
                                        lvKvizovi.setAdapter(adapter);
                                    }).execute(" ");
                                }
                            }

                            public void onNothingSelected(AdapterView<?> arg0) {
                                Resources res = getResources();
                                novaListaKvizova = new ArrayList<>();
                                for (int i = 0; i < kvizovi.size(); i++)
                                    novaListaKvizova.add(kvizovi.get(i));
                                adapter = new DodajKvizAdapter(KvizoviAkt.this, kvizovi, res);
                                lvKvizovi.setAdapter(adapter);
                            }
                        });


                        lvKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                            @Override
                            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long id) {
                                if (konekcija) {
                                    if (novaListaKvizova.size() == 0) {
                                        if (position != novaListaKvizova.size() - 1) {
                                            pozicijaKvizaKojegUredujemo = position;
                                            uredivanjeKviza = true;
                                            Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                            myIntent.putExtra("naziv", kvizovi.get(position).getNaziv());
                                            myIntent.putExtra("listaPitanja", kvizovi.get(position).getPitanja());
                                            myIntent.putExtra("kategorija", kvizovi.get(position).getKategorija().getNaziv());
                                            startActivity(myIntent);
                                            KvizoviAkt.this.startActivityForResult(myIntent, 0);
                                        } else if (position == novaListaKvizova.size() - 1) {
                                            Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                            KvizoviAkt.this.startActivityForResult(myIntent, 0);
                                        }
                                    } else {
                                        if (position != novaListaKvizova.size() - 1) {
                                            Iterator iter = kvizoviIzBaze.entrySet().iterator();
                                            while (iter.hasNext()) {
                                                Map.Entry par = (Map.Entry) iter.next();
                                                Kviz k = (Kviz) par.getKey();
                                                if (k.getNaziv().equals(novaListaKvizova.get(position).getNaziv())) {
                                                    String idKvizUredivanje = (String) (par.getValue());
                                                    int i;
                                                    for (i = idKvizUredivanje.length() - 1; i > 0; i--) {
                                                        if (idKvizUredivanje.charAt(i) == '/')
                                                            break;
                                                    }
                                                    i++;
                                                    idKvizaZaUredivanje = idKvizUredivanje.substring(i, idKvizUredivanje.length());

                                                }

                                            }

                                            pozicijaKvizaKojegUredujemo = position;
                                            uredivanjeKviza = true;
                                            Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                            myIntent.putExtra("naziv", novaListaKvizova.get(position).getNaziv());
                                            myIntent.putExtra("listaPitanja", novaListaKvizova.get(position).getPitanja());
                                            myIntent.putExtra("kategorija", novaListaKvizova.get(position).getKategorija().getNaziv());
                                            KvizoviAkt.this.startActivityForResult(myIntent, 0);
                                        } else if (position == novaListaKvizova.size() - 1) {
                                            Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                            KvizoviAkt.this.startActivityForResult(myIntent, 0);
                                        }
                                    }
                                    return true;
                                }
                                return false;

                            }
                        });


                }).execute(" ");
            }

        }
    }

   @Override
    public void onItemSelected(AdapterView parent, View view, int pos,
        long id) {
        Toast.makeText(getApplicationContext(), spPostojeceKategorije.getItemAtPosition(pos).toString(), Toast.LENGTH_LONG).show();
        }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean dodatiKategoriju = true;
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                if (uredivanjeKviza) {
                    String nazivKviza = data.getStringExtra("nazivKviza");
                    ArrayList<Pitanje> pitanja = (ArrayList<Pitanje>) data.getSerializableExtra("pitanja");
                    pitanja.remove(pitanja.size() - 1);
                    int duzinaPitanja = pitanja.size();
                    Kategorija kat = new Kategorija(data.getStringExtra("kategorija"), data.getStringExtra("idKategorije"));
                    novaListaKvizova.remove(pozicijaKvizaKojegUredujemo);
                    kvizovi.remove(pozicijaKvizaKojegUredujemo);
                    novaListaKvizova.add(pozicijaKvizaKojegUredujemo, new Kviz(nazivKviza, pitanja, kat));
                    kvizovi.add(pozicijaKvizaKojegUredujemo, new Kviz(nazivKviza, pitanja, kat));
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < kategorije.size(); i++) {
                        if (kat.getNaziv().equals(kategorije.get(i).getNaziv()))
                            dodatiKategoriju = false;
                    }
                    if (dodatiKategoriju) {
                        kategorije.add(kat);
                        adapter2.notifyDataSetChanged();
                    }
                    //DODANO
                    if (konekcija){
                        ucitajUSQLBazu();
                    }

                    uredivanjeKviza = false;
                } else {
                    String nazivKviza = data.getStringExtra("nazivKviza");
                    ArrayList<Pitanje> pitanja = (ArrayList<Pitanje>) data.getSerializableExtra("pitanja");
                    pitanja.remove(pitanja.size() - 1);
                    Kategorija kat = new Kategorija(data.getStringExtra("kategorija"), data.getStringExtra("idKategorije"));
                    novaListaKvizova.add(novaListaKvizova.size() - 1, new Kviz(nazivKviza, pitanja, kat));
                    kvizovi.add(kvizovi.size() - 1, new Kviz(nazivKviza, pitanja, kat));
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < kategorije.size(); i++) {
                        if (kat.getNaziv().equals(kategorije.get(i).getNaziv()))
                            dodatiKategoriju = false;
                    }
                    if (dodatiKategoriju) {
                        kategorije.add(kat);
                        adapter2.notifyDataSetChanged();
                    }
                    //DODANO
                    if (konekcija){
                        ucitajUSQLBazu();
                    }

                }
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == 98) {
            if (resultCode == RESULT_OK) {
                if (uredivanjeKviza) {
                    String nazivKviza = data.getStringExtra("nazivKviza");
                    ArrayList<Pitanje> pitanja = (ArrayList<Pitanje>) data.getSerializableExtra("pitanja");
                    pitanja.remove(pitanja.size() - 1);
                    Kategorija kat = new Kategorija(data.getStringExtra("kategorija"), data.getStringExtra("idKategorije"));
                    novaListaKvizova.remove(pozicijaKvizaKojegUredujemo);
                    novaListaKvizova.add(pozicijaKvizaKojegUredujemo, new Kviz(nazivKviza, pitanja, kat));
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("listaKvizoveGrid", novaListaKvizova);
                    DetailFrag detailFrag = new DetailFrag();
                    detailFrag.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.detailPlace, detailFrag).commitAllowingStateLoss();
                    ListaFrag.kategorijaAdapter.notifyDataSetChanged();
                    DetailFrag.gridViewAdapter.notifyDataSetChanged();
                    for (int i = 0; i < kategorije.size(); i++) {
                        if (kat.getNaziv().equals(kategorije.get(i).getNaziv()))
                            dodatiKategoriju = false;
                    }
                    if (dodatiKategoriju) {
                        kategorije.add(kat);
                        ListaFrag.kategorijaAdapter.notifyDataSetChanged();
                    }
                    ListaFrag listaFrag = new ListaFrag();
                    bundle.putSerializable("sveKategorijeZaGrid", kategorije);
                    listaFrag.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.listPlace,listaFrag);
                    uredivanjeKviza = false;
                    //DODANO
                    if (konekcija){
                        ucitajUSQLBazu();
                    }

                }
                else {
                    String nazivKviza = data.getStringExtra("nazivKviza");
                    ArrayList<Pitanje> pitanja = (ArrayList<Pitanje>) data.getSerializableExtra("pitanja");
                    pitanja.remove(pitanja.size() - 1);
                    Kategorija kat = new Kategorija(data.getStringExtra("kategorija"), data.getStringExtra("idKategorije"));
                    novaListaKvizova.add(novaListaKvizova.size() - 1, new Kviz(nazivKviza, pitanja, kat));
                    kvizovi.add(kvizovi.size() - 1, new Kviz(nazivKviza, pitanja, kat));
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("listaKvizoveGrid", novaListaKvizova);
                    DetailFrag detailFrag = new DetailFrag();
                    detailFrag.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.detailPlace, detailFrag).commitAllowingStateLoss();
                    ListaFrag.kategorijaAdapter.notifyDataSetChanged();
                    DetailFrag.gridViewAdapter.notifyDataSetChanged();
                    for (int i = 0; i < kategorije.size(); i++) {
                        if (kat.getNaziv().equals(kategorije.get(i).getNaziv()))
                            dodatiKategoriju = false;
                    }
                    if (dodatiKategoriju) {
                        kategorije.add(kat);
                        ListaFrag.kategorijaAdapter.notifyDataSetChanged();
                    }
                    ListaFrag listaFrag = new ListaFrag();
                    bundle.putSerializable("sveKategorijeZaGrid", kategorije);
                    listaFrag.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.listPlace,listaFrag);
                    //DODANO
                    if (konekcija){
                        ucitajUSQLBazu();
                    }


                }
            }
        }
    }



    @Override
    public void selektovano(Kategorija kategorija, int pozicija ) {
        Bundle bundle = new Bundle();
        filtriranaLista = new ArrayList<>();
        if (pozicija == 0){
            for (int i = 0; i < novaListaKvizova.size() - 1; i++){
                filtriranaLista.add(novaListaKvizova.get(i));
            }
        } else {
            for (int i = 0; i < novaListaKvizova.size(); i++){
                if (novaListaKvizova.get(i).getKategorija().getNaziv().equals(kategorija.getNaziv()))
                    if (!novaListaKvizova.get(i).getNaziv().equals(kvizDodaj.getNaziv())) {
                        filtriranaLista.add(novaListaKvizova.get(i));
                    }
            }}
        filtriranaLista.add(kvizDodaj);
        bundle.putParcelableArrayList("listaKvizoveGrid", filtriranaLista);
        DetailFrag detailFrag = new DetailFrag();
        detailFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.detailPlace, detailFrag).commitAllowingStateLoss();

    }
}
