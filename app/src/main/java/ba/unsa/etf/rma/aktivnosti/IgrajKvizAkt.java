package ba.unsa.etf.rma.aktivnosti;


import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.AlarmClock;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.fragmenti.PitanjeFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class IgrajKvizAkt extends AppCompatActivity implements PitanjeFrag.SlanjeVrijednosti, InformacijeFrag.Kraj {

    private static final int MY_CAL_REQ = 0;
    public static Kviz pokrenutiKviz = new Kviz();
    Pitanje randomPitanje = new Pitanje();
    public static ArrayList<Pitanje> odigranaPitanja = new ArrayList<>();
    public static ArrayList<Pitanje> svaPitanja = new ArrayList<>();
    public static ArrayList<Pitanje> preostalaPitanja = new ArrayList<>();
    private ArrayList<String> odgovori = new ArrayList<>();
    private int brojPreostalihPitanja = 0;
    private int brojTacnihOdgovora = 0;
    private double procenatTacnihOdgovora = 0;
    private int brojOdigranih = 0;
    private int ukupanBrojPitanja;
    private double X;
    private int N;


    public void kreirajAlarm(String message, int hour, int minutes){
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM).
                putExtra(AlarmClock.EXTRA_SKIP_UI, true).
                putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if (intent.resolveActivity(getPackageManager())!= null){
            startActivity(intent);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igraj_kviz_akt);
        FragmentManager fragmentMenager = getSupportFragmentManager();
        FrameLayout layoutInformacija=(FrameLayout)findViewById(R.id.informacijePlace);

            Intent intent = getIntent();
            String nazivKviza = intent.getStringExtra("naziv");
            svaPitanja = (ArrayList<Pitanje>) intent.getSerializableExtra("listaPitanja");
            preostalaPitanja = svaPitanja;
            Kategorija kategorija = new Kategorija(intent.getStringExtra("kategorija"), intent.getStringExtra("idKategorije"));
            pokrenutiKviz.setNaziv(nazivKviza);
            pokrenutiKviz.setKategorija(kategorija);
            Collections.shuffle(svaPitanja);

            N = svaPitanja.size();
            X = N / 2.;
            if (X != 0) {
                int sati = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                double minute = Calendar.getInstance().get(Calendar.MINUTE) + X;
                if (minute - (int) minute >= 0.5) minute = (int) minute + 1;
                else minute = (int) (minute);

                if (minute > 60) {
                    minute = minute - 60;
                    sati++;
                }
                kreirajAlarm(" ", sati, (int) minute);
            }
            else {
                Toast.makeText(getApplicationContext(), "NEMA PITANJA U KVIZU! ALARM NIJE AKTIVIRAN!", Toast.LENGTH_SHORT).show();
            }
            if (svaPitanja.size()==0){
                randomPitanje.setTekstPitanja("Kviz je završen!");
                randomPitanje.setNaziv("Kviz je završen!");
                randomPitanje.setOdgovori(new ArrayList<>());
            }
           else {
               randomPitanje = svaPitanja.get(0);
               preostalaPitanja.remove(0);
           }
           if (randomPitanje.getNaziv().equals("Kviz je završen!")){
               AlertDialog.Builder alertDialog = new AlertDialog.Builder(IgrajKvizAkt.this);
               alertDialog.setTitle("UNOS");
               alertDialog.setMessage("Unesite ime: ");
               final EditText input = new EditText(IgrajKvizAkt.this);
               LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                       LinearLayout.LayoutParams.MATCH_PARENT,
                       LinearLayout.LayoutParams.MATCH_PARENT);
               input.setLayoutParams(lp);
               alertDialog.setView(input);
               alertDialog.setIcon(R.drawable.greska);
               alertDialog.setPositiveButton("OK",
                       new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int which) {
                               Bundle arg = new Bundle();
                               RangLista df = new RangLista();
                               arg.putString("nazivKviza", pokrenutiKviz.getNaziv());
                               arg.putString("igrac", input.getText().toString());
                               arg.putString("procenatTacnih", String.valueOf(procenatTacnihOdgovora));
                               df.setArguments(arg);
                               getSupportFragmentManager().beginTransaction().replace(R.id.pitanjePlace, df).commit();
                           }
                       });
               alertDialog.show();}
            ukupanBrojPitanja = svaPitanja.size();
            brojPreostalihPitanja = svaPitanja.size();
            odigranaPitanja.add(randomPitanje);



        if(layoutInformacija != null) {
            InformacijeFrag infoFrag;
            infoFrag=(InformacijeFrag) fragmentMenager.findFragmentById(R.id.informacijePlace);
            if(infoFrag==null) {
                infoFrag=new InformacijeFrag();
                Bundle bundle = new Bundle();
                bundle.putString("nazivKviza", pokrenutiKviz.getNaziv());
                bundle.putString("brojPreostalih", String.valueOf(brojPreostalihPitanja));
                bundle.putString("brojTacnih", String.valueOf(brojTacnihOdgovora));
                bundle.putString("procenatTacnih", String.valueOf(procenatTacnihOdgovora));
                infoFrag.setArguments(bundle);
                fragmentMenager.beginTransaction().replace(R.id.informacijePlace,infoFrag).commit();
            }
            else {
                fragmentMenager.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }

        FrameLayout layoutPitanje=(FrameLayout)findViewById(R.id.pitanjePlace);

        if(layoutPitanje != null) {
            PitanjeFrag pitanjeFrag;
            pitanjeFrag=(PitanjeFrag) fragmentMenager.findFragmentById(R.id.pitanjePlace);
            if(pitanjeFrag==null) {
                pitanjeFrag=new PitanjeFrag();
                Bundle bundle = new Bundle();
                bundle.putString("tekstPitanja", randomPitanje.getNaziv());
                bundle.putStringArrayList("odgovoriNaPitanje", randomPitanje.getOdgovori());
                bundle.putString("tacanOdgovor", randomPitanje.getTacan());
                pitanjeFrag.setArguments(bundle);
                fragmentMenager.beginTransaction().replace(R.id.pitanjePlace,pitanjeFrag).commit();
            }
            else {
                fragmentMenager.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }


    }

    @Override
    public void promjenaVrijednosti() {
        InformacijeFrag informacijeFrag = new InformacijeFrag();
        Bundle bundle = new Bundle();
        bundle.putString("nazivKviza", pokrenutiKviz.getNaziv());
        bundle.putString("brojPreostalih", String.valueOf(brojPreostalihPitanja));
        bundle.putString("brojTacnih", String.valueOf(brojTacnihOdgovora));
        bundle.putString("procenatTacnih", String.valueOf(procenatTacnihOdgovora));
        informacijeFrag.setArguments(bundle);
        InformacijeFrag informacijeFrag1 = new InformacijeFrag();
        informacijeFrag1.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.informacijePlace,informacijeFrag1).commit();

    }

    @Override
    public void kraj() {
        super.onBackPressed();
    }

    @Override
    public void promjenaPitanja(int tacanOdgovor) {
        brojPreostalihPitanja--;
        if (brojPreostalihPitanja < 0) brojPreostalihPitanja = 0;
        brojOdigranih++;
        if (tacanOdgovor == 13){
            brojTacnihOdgovora++;
        }
        procenatTacnihOdgovora = (double) brojTacnihOdgovora / brojOdigranih;
        if (preostalaPitanja.size() == 0) {
            randomPitanje.setTekstPitanja("Kviz je završen!");
            randomPitanje.setNaziv("Kviz je završen!");
            randomPitanje.setOdgovori(new ArrayList<>());
        }
            else {
            randomPitanje = preostalaPitanja.get(0);
            preostalaPitanja.remove(0);
            odigranaPitanja.add(randomPitanje);
        }
        if (randomPitanje.getNaziv().equals("Kviz je završen!")){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IgrajKvizAkt.this);
            alertDialog.setTitle("UNOSITE");
            alertDialog.setMessage("Unesite ime: ");
            final EditText input = new EditText(IgrajKvizAkt.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            alertDialog.setView(input);
            alertDialog.setIcon(R.drawable.greska);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Bundle arg = new Bundle();
                            RangLista df = new RangLista();
                            arg.putString("nazivKviza", pokrenutiKviz.getNaziv());
                            arg.putString("igrac", input.getText().toString());
                            arg.putString("procenatTacnih", String.valueOf(procenatTacnihOdgovora));
                            df.setArguments(arg);
                            getSupportFragmentManager().beginTransaction().replace(R.id.pitanjePlace, df).commit();
                        }
                    });
            alertDialog.show();}
        PitanjeFrag pitanjeFrag = new PitanjeFrag();
        pitanjeFrag=new PitanjeFrag();
        Bundle bundle = new Bundle();
        bundle.putString("tekstPitanja", randomPitanje.getNaziv());
        bundle.putStringArrayList("odgovoriNaPitanje", randomPitanje.getOdgovori());
        bundle.putString("tacanOdgovor", randomPitanje.getTacan());
        pitanjeFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.pitanjePlace,pitanjeFrag).commit();

    }

}
