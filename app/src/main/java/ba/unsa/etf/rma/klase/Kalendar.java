package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.provider.CalendarContract;

public class Kalendar {

    Context context;
    public Kalendar(Context context){this.context = context;}

    public static final String[] vrijednosti =
            new String[]{
                    CalendarContract.Events.DTSTART,
                    CalendarContract.Events.TITLE
            };
}
