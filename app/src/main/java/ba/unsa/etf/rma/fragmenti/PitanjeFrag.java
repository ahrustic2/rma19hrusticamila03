package ba.unsa.etf.rma.fragmenti;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class PitanjeFrag extends Fragment {

    private TextView tekstPitanja;
    private ListView odgovoriPitanja;
    private String tacanOdgovor;
    private ArrayList<String> odgovori = new ArrayList<>();
    private ArrayAdapter<String> odgovoriAdapter;
    private int pozicijaTacnogOdgovora = -5;
    private int pozicijaNetacnogOdgovora = -13;
    private int brojTacnihOdgovora = 0;
    private int brojOdigranih = 0;
    SlanjeVrijednosti slanjeVrijednosti;
    int daLiJeOdgovorTacan = 0;

    public interface SlanjeVrijednosti{
        public void promjenaVrijednosti();
        public void promjenaPitanja(int tacan);
    }

    public PitanjeFrag() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments().containsKey("tekstPitanja")){
            tekstPitanja = (TextView) getView().findViewById(R.id.id);
            odgovoriPitanja = (ListView) getView().findViewById(R.id.odgovoriPitanja);
            tekstPitanja.setText(getArguments().getString("tekstPitanja"));
            odgovori = getArguments().getStringArrayList("odgovoriNaPitanje");
            tacanOdgovor = getArguments().getString("tacanOdgovor");
                odgovoriAdapter = new ArrayAdapter<String>(getActivity(), R.layout.element, R.id.etNaziv, odgovori) {
                    @Override
                    public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        String odgovor = odgovori.get(position);
                        TextView naziv = (TextView) view.findViewById(R.id.etNaziv);
                        naziv.setText(odgovor);
                        naziv.setBackgroundColor(0);
                        if (pozicijaTacnogOdgovora == -1) {
                            if (pozicijaNetacnogOdgovora == position) {
                                naziv.setBackgroundColor(getResources().getColor(R.color.crvena));
                            }
                            else if(odgovori.get(position).equals(tacanOdgovor)){
                                naziv.setBackgroundColor(getResources().getColor(R.color.zelena));}
                        } else if (pozicijaTacnogOdgovora >= 0) {
                            if (position == pozicijaTacnogOdgovora) {
                                naziv.setBackgroundColor(getResources().getColor(R.color.zelena));
                                daLiJeOdgovorTacan = 13;

                            }
                        }


                        return view;
                    }
                };
                odgovoriPitanja.setAdapter(odgovoriAdapter);
        }
        slanjeVrijednosti.promjenaVrijednosti();
        odgovoriPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(getArguments().getString("tacanOdgovor").equals(odgovori.get(position))){
                    pozicijaTacnogOdgovora = position;
                    odgovoriAdapter.notifyDataSetChanged();
                }
                else {
                    pozicijaTacnogOdgovora = -1;
                    pozicijaNetacnogOdgovora = position;
                    odgovoriAdapter.notifyDataSetChanged();
                }
                odgovoriPitanja.setEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        slanjeVrijednosti.promjenaPitanja(daLiJeOdgovorTacan);}}, 2000);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pitanje, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;

        try {
            slanjeVrijednosti = (SlanjeVrijednosti) activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }

}
