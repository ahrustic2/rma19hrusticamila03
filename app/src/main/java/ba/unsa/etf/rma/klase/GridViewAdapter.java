package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class GridViewAdapter extends BaseAdapter implements View.OnClickListener{
    private Activity aktivnost;
    private ArrayList lista;
    private static LayoutInflater inflater = null;
    public Resources res;
    Kviz kviz = null;
    int brojac = 0;

    public GridViewAdapter(Activity kvizoviAkt, ArrayList<Kviz> kategorije, Resources resources) {
        aktivnost = kvizoviAkt;
        lista = kategorije;
        res = resources;
        inflater = ( LayoutInflater )aktivnost.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public int getCount() {
        if(lista.size()<=0)
            return 1;
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        public TextView naziv;
        public IconView icon;
        public TextView brojPitanja;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        GridViewAdapter.ViewHolder holder;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.element_grid, null);
            holder = new GridViewAdapter.ViewHolder();
            holder.icon = (IconView) vi.findViewById(R.id.icon);
            holder.naziv = (TextView) vi.findViewById(R.id.etNaziv);
            holder.brojPitanja = (TextView) vi.findViewById(R.id.brojPitanja);
            vi.setTag(holder);
        } else {
            holder = (GridViewAdapter.ViewHolder) vi.getTag();
        }
        if (lista.size() <= 0) {
            holder.naziv.setText("Prazno");
            holder.brojPitanja.setText(R.string.nema_info);
        } else {
            kviz = (Kviz) lista.get(position);
            holder.naziv.setText(kviz.getNaziv());
            holder.icon.setIcon(Integer.parseInt(kviz.getKategorija().getId()));
            holder.brojPitanja.setText(String.valueOf(kviz.getPitanja().size()));
            if (lista.size() - 1 == position){
                holder.brojPitanja.setText("");
            }
        }
        return vi;
    }
}
