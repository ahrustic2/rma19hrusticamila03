package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AsyncResponse;
import ba.unsa.etf.rma.klase.DodajKategorijaAdapter;
import ba.unsa.etf.rma.klase.DodajPitanjeAdapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodajKvizAkt extends AppCompatActivity {

    private Spinner spKategorije;
    private EditText etNaziv;
    private ListView lvDodanaPitanja;
    private ListView lvMogucaPitanja;
    private Button btnDodajKviz;
    private ArrayList<Pitanje> dodajPitanje = new ArrayList<Pitanje>();
    private ArrayList<Pitanje> mogucePitanje = new ArrayList<Pitanje>();
    private DodajPitanjeAdapter adapter;
    private DodajPitanjeAdapter adapterMogucePitanje;
    private DodajKategorijaAdapter adapterKategorija;
    private ArrayList<Kategorija> kategorije = new ArrayList<>();
    String kategorija;
    public static ArrayList<String> pomocniKategorije = new ArrayList<>();
    boolean sredivanjeKviza = false;
    private Button btnImportKviz;
    private static final int READ_REQUEST_CODE = 13;
    private static final int WRITE_REQUEST_CODE = 10;
    private Kviz kviz = new Kviz();
    private int brojOdgovora = 0;
    private String idKategorijeIzBaze;
    public static Map<Pitanje, String> pitanjaUKvizuIzBaze = new HashMap<>();
    public static Map<Pitanje, String> mogucaPitanjaBaza = new HashMap<>();
    int brojac = 0;
    private KvizoviAkt kvizoviAkt;
    DodajPitanjeAkt dodavanjePitanja;
    public static Map<Pitanje, String> mogucaPitanjaIzBaze = new HashMap<>();
    //public static Map<String, String> listaKategorijaIzBaze;
    public static Map<Pitanje, String> mapaPitanjaVezanihZaKviz = new HashMap<>();
    public static ArrayList<String> listaIdPitanjaVezanihZaBazu = new ArrayList<>();
    private ArrayList<Pitanje> pocetnaListaPitanja = new ArrayList<>();
    public static ArrayList<String> idPitanjaIzBaze = new ArrayList<>();


    public class DodajKviz extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            {
                InputStream stream = getResources().openRawResource(R.raw.secret);

                try {
                    GoogleCredential credentials = GoogleCredential.fromStream(stream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                    credentials.refreshToken();
                    String TOKEN = credentials.getAccessToken();
                    System.out.println("SADA JE TOKEN: " + TOKEN);
                    String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Kvizovi?access_token=";
                    URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                    HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("Accept", "application/json");

                    Iterator it = pitanjaUKvizuIzBaze.entrySet().iterator();
                    String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+etNaziv.getText().toString()+"\"}";
                    if (kvizoviAkt.listaKategorijaIzBaze.size() != 0){

                        System.out.println("ULAZIIII111...");
                        dokument += ", \"idKategorije\": ";
                        Iterator iterator = KvizoviAkt.listaKategorijaIzBaze.entrySet().iterator();
                        while(iterator.hasNext())
                        {
                            Map.Entry pair = (Map.Entry) iterator.next();
                            Kategorija kat = (Kategorija) pair.getKey();
                            String proba = (String) pair.getValue();
                            int k;
                            boolean imaCrticu = false;
                            for(k = proba.length() - 1; k > 0; k--) {
                                if (proba.charAt(k) == '/') {
                                    imaCrticu = true;
                                    break;
                                }
                            }
                            if (imaCrticu) k++;
                            proba = proba.substring(k, proba.length());
                            if (spKategorije.getSelectedItemPosition() == 1){
                                dokument += "{\"stringValue\" : \"" + "idKat" + "\"}";
                                break;}
                            else if (kat.getNaziv().equals(kviz.getKategorija().getNaziv())){
                                dokument += "{\"stringValue\" : \"" + proba + "\"}";
                                break;
                            }

                        }
                    }
                    else if (kviz.getKategorija().getNaziv().equals("Svi")){
                        dokument += ", \"idKategorije\": { \"stringValue\" : \"idKat\"}";
                    }
                    dokument +=" , \"pitanja\" : { \"arrayValue\" : { \"values\" : [";
                    if (dodajPitanje.size() == 0) dokument += "{\"stringValue\" : \"" + "" + "\"}";
                    else {
                        String proba = "";

                        System.out.println("ULAZIIII111...");
                        for (int l = 0; l < dodajPitanje.size(); l++)
                        {

                            Iterator krozBazuPitanja = KvizoviAkt.pitanjaIzBaze.entrySet().iterator();
                            while(krozBazuPitanja.hasNext()) {
                                Map.Entry ppp = (Map.Entry) krozBazuPitanja.next();
                                Pitanje pitanje = (Pitanje) ppp.getKey();
                                pitanje.setImage(R.drawable.blue_icon);
                                {
                                    if (dodajPitanje.get(l).getNaziv().equals(pitanje.getNaziv())) {
                                        proba = (String) ppp.getValue();
                                        boolean imaCrticu = false;
                                        int k;
                                        for (k = proba.length() - 1; k > 0; k--) {
                                            if (proba.charAt(k) == '/'){
                                                imaCrticu = true;
                                                break;
                                            }
                                        }
                                        if(imaCrticu) k++;
                                        proba = proba.substring(k, proba.length());
                                    }

                                }
                            }
                            if(l != dodajPitanje.size() - 1) {
                                dokument += "{\"stringValue\" : \"" + proba + "\"},";
                            }
                            else{

                                System.out.println("ULAZIIII111...");
                                dokument += "{\"stringValue\" : \"" + proba + "\"}";
                            }
                        }
                    }
                    dokument+= "]}}}}";


                    System.out.println(dokument.toString());

                    try (OutputStream os = connection.getOutputStream()) {
                        byte[] input = dokument.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }

                    int code = connection.getResponseCode();
                    InputStream odgovor = connection.getInputStream();
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(odgovor, "utf-8"))
                    ) {
                        StringBuilder response = new StringBuilder();
                        String rensponseLine = null;
                        while ((rensponseLine = br.readLine()) != null) {
                            response.append(rensponseLine.trim());
                        }
                        Log.d("ODGOVOR", response.toString());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

        }
    }



    public class UrediKviz extends AsyncTask<String, Void, Void> {

        //public interface O{public void izbrsi();}
        AsyncResponse delegat = null;

        public UrediKviz(AsyncResponse delegat){this.delegat = delegat;}

        @Override
        protected void onPostExecute(Void aVoid) {
            //super.onPostExecute(aVoid);

        }

        @Override
        protected Void doInBackground(String... strings) {
            InputStream stream = getResources().openRawResource(R.raw.secret);

            try {
                GoogleCredential credential = GoogleCredential.fromStream(stream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credential.refreshToken();
                String TOKEN = credential.getAccessToken();
                Log.d("Sada je token: ", TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Kvizovi/" + kvizoviAkt.idKvizaZaUredivanje + "?access_token=";

                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PATCH");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                Iterator it = pitanjaUKvizuIzBaze.entrySet().iterator();
                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+etNaziv.getText().toString()+"\"}";
                if (kvizoviAkt.listaKategorijaIzBaze.size() != 0){
                    dokument += ", \"idKategorije\": ";
                    Iterator iterator = KvizoviAkt.listaKategorijaIzBaze.entrySet().iterator();
                    while(iterator.hasNext())
                    {
                        Map.Entry pair = (Map.Entry) iterator.next();
                        Kategorija kat = (Kategorija) pair.getKey();
                        String proba = (String) pair.getValue();
                        int k;
                        boolean imaCrta = false;
                        for(k = proba.length() - 1; k > 0; k--) {
                            if (proba.charAt(k) == '/'){
                                imaCrta = true;
                                break;
                            }
                        }
                        if (imaCrta) k++;
                        proba = proba.substring(k, proba.length());
                        if (kat.getNaziv().equals(kviz.getKategorija().getNaziv())){
                            dokument += "{\"stringValue\" : \"" + proba + "\"}";
                            break;
                        }
                    }
                }
                else {
                    dokument += ", \"idKategorije\": { \"stringValue\" : \"idKat\"}";
                }
                dokument +=" , \"pitanja\" : { \"arrayValue\" : { \"values\" : [";
                if (dodajPitanje.size() == 0) dokument += "{\"stringValue\" : \"" + "" + "\"}";
                else {
                    String proba = "";
               for (int l = 0; l < dodajPitanje.size(); l++)
                    {
                        boolean postoji = false;
                        Iterator krozBazuPitanja = KvizoviAkt.pitanjaIzBaze.entrySet().iterator();
                        while(krozBazuPitanja.hasNext()) {
                            Map.Entry ppp = (Map.Entry) krozBazuPitanja.next();
                            Pitanje pitanje = (Pitanje) ppp.getKey();
                            pitanje.setImage(R.drawable.blue_icon);
                            {
                                if (dodajPitanje.get(l).getNaziv().equals(pitanje.getNaziv())) {
                                    proba = (String) ppp.getValue();
                                    int k;
                                    boolean imaCrticu = false;
                                    for (k = proba.length() - 1; k > 0; k--) {
                                        if (proba.charAt(k) == '/'){
                                            imaCrticu = true;
                                            break;
                                        }
                                    }
                                    if (imaCrticu) k++;
                                    proba = proba.substring(k, proba.length());
                                }

                            }
                        }
                        for (int j = 0; j < pocetnaListaPitanja.size(); j++) {
                            if (pocetnaListaPitanja.get(j) == dodajPitanje.get(l)) postoji = true;
                        }
                        if (!postoji){
                            if(l != dodajPitanje.size() - 1) {
                                dokument += "{\"stringValue\" : \"" + proba + "\"},";
                            }
                            else {
                                dokument += "{\"stringValue\" : \"" + proba + "\"}";
                            }
                        }
                    }
                }
                dokument+= "]}}}}";

                try (OutputStream os = connection.getOutputStream())
                {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    StringBuilder response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    public class UcitajPitanjaZaKviz extends AsyncTask<String, Void, Void>{


        AsyncResponse odgovor = null;

        public UcitajPitanjaZaKviz(AsyncResponse asyncResponse) {
            odgovor = asyncResponse;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            odgovor.zavrsenProces("");
        }

        @Override
        protected Void doInBackground(String... strings) {
            InputStream stream = getResources().openRawResource(R.raw.secret);

            try {
                dodajPitanje.clear();
                GoogleCredential credential = GoogleCredential.fromStream(stream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credential.refreshToken();
                String TOKEN = credential.getAccessToken();
                Log.d("Sada je token: ", TOKEN);
                String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Kvizovi/" + kvizoviAkt.idKvizaZaUredivanje + "?access_token=";
                URL urlObjPitanje = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connectionP  = (HttpURLConnection) urlObjPitanje.openConnection();
                connectionP.connect();
                InputStream odgovorP = connectionP.getInputStream();
                StringBuilder responseP = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovorP, "utf-8"))
                ){
                    responseP = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        responseP.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR PITANJE: ", responseP.toString());
                }
                String rezultatP = responseP.toString();
                JSONObject jsonObjectP = new JSONObject(rezultatP);
                    String nameP = jsonObjectP.getString("name");
                    JSONObject fieldsP = jsonObjectP.getJSONObject("fields");
                    JSONObject pitanja = fieldsP.getJSONObject("pitanja");
                    JSONObject arrayValue = pitanja.getJSONObject("arrayValue");
                     JSONArray values = arrayValue.getJSONArray("values");
                     for (int j = 0; j < values.length(); j++) {
                    JSONObject item = values.getJSONObject(j);
                    String pit = item.getString("stringValue");
                    listaIdPitanjaVezanihZaBazu.add(pit);
                 }

                boolean postojiDodaj = false;
                for (int i = 0; i < dodajPitanje.size(); i++){
                    if (dodajPitanje.get(i).getNaziv().equals("Dodaj pitanje")) postojiDodaj = true;
                }
                if (!postojiDodaj)  dodajPitanje.add(new Pitanje("Dodaj pitanje", R.drawable.plus_icon));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dodajPitanje.clear();
        pocetnaListaPitanja.clear();
        listaIdPitanjaVezanihZaBazu.clear();
        pitanjaUKvizuIzBaze.clear();
        setContentView(R.layout.activity_dodaj_kviz_akt);
        lvDodanaPitanja = (ListView) findViewById(R.id.lvDodanaPitanja);
        lvMogucaPitanja = (ListView) findViewById(R.id.lvMogucaPitanja);
        spKategorije = (Spinner) findViewById(R.id.spKategorije);
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        btnDodajKviz = (Button) findViewById(R.id.btnDodajKviz);
        btnImportKviz = (Button) findViewById(R.id.btnImportKviz);
        etNaziv.setHint("Naziv kviza - INPUT");
        for (int i = 0; i < KvizoviAkt.kategorije.size(); i++){
            kategorije.add(KvizoviAkt.kategorije.get(i));
        }
        kategorije.add(new Kategorija("Dodaj kategoriju","plus_icon"));
        if (getIntent().getExtras() != null){
        new UcitajPitanjaZaKviz(new AsyncResponse() {
            @Override
            public void zavrsenProces(String izlaz) {
                if (getIntent().getExtras() != null){
                    Iterator krozBazuPitanja = KvizoviAkt.pitanjaIzBaze.entrySet().iterator();
                    while(krozBazuPitanja.hasNext()){
                        Map.Entry pair = (Map.Entry) krozBazuPitanja.next();
                        Pitanje pitanje = (Pitanje) pair.getKey();
                        String proba = (String) pair.getValue();
                        int k;
                        for(k = proba.length() - 1; k > 0; k--) {
                            if (proba.charAt(k) == '/')
                                break;
                        }
                        k++;
                        proba = proba.substring(k, proba.length());
                        boolean postojiPitanje = false;

                        for (int i = 0; i < listaIdPitanjaVezanihZaBazu.size(); i++){
                            //System.out.println("---------->" + (String) pair.getValue());
                            if (listaIdPitanjaVezanihZaBazu.get(i).equals(proba)){
                                postojiPitanje = true;
                                pitanje.setImage(R.drawable.blue_icon);
                                System.out.println("ULAZIII" );
                                dodajPitanje.add(dodajPitanje.size()-1,pitanje);
                                break;
                            }
                        }
                        if (!postojiPitanje){
                            pitanje.setImage(R.drawable.plus_icon);
                            mogucePitanje.add(pitanje);

                        }
                        postojiPitanje = false;
                    }

                    adapterMogucePitanje.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                }


                adapterMogucePitanje.notifyDataSetChanged();
                adapter.notifyDataSetChanged();

            }
        }).execute("");
        }
        if (getIntent().getExtras() == null){
            Iterator krozBazuPitanja = KvizoviAkt.pitanjaIzBaze.entrySet().iterator();
            while(krozBazuPitanja.hasNext()) {
                Map.Entry pair = (Map.Entry) krozBazuPitanja.next();
                Pitanje pitanje = (Pitanje) pair.getKey();
                pitanje.setImage(R.drawable.plus_icon);
                mogucePitanje.add(pitanje);

            }
        }
        Resources res = getResources();
        adapter = new DodajPitanjeAdapter(this, dodajPitanje, res);
        adapterMogucePitanje = new DodajPitanjeAdapter(this, mogucePitanje, res);
        lvMogucaPitanja.setAdapter(adapterMogucePitanje);
        lvDodanaPitanja.setAdapter(adapter);
        adapterMogucePitanje.notifyDataSetChanged();
        adapter.notifyDataSetChanged();

        adapterKategorija = new DodajKategorijaAdapter(this, kategorije, res);
        spKategorije.setAdapter(adapterKategorija);
        //za spajanje sa kvizom
        //KATEGORIJA
        if (getIntent().getExtras() != null) {
            etNaziv.setText(getIntent().getStringExtra("naziv"));
            ArrayList<Pitanje> pitanjaKviza = (ArrayList<Pitanje>)getIntent().getSerializableExtra("listaPitanja");
            for(int i = 0; i < pitanjaKviza.size(); i++) dodajPitanje.add(new Pitanje(pitanjaKviza.get(i).getNaziv(), R.drawable.blue_icon));
            int pozicija = 0;
            for (int i=0; i < kategorije.size(); i++){
                if (kategorije.get(i).getNaziv().equals(getIntent().getStringExtra("kategorija"))){
                    pozicija = i;
                }
            }


            spKategorije.setSelection(pozicija);
            adapterKategorija.notifyDataSetChanged();
            sredivanjeKviza = true;


        }


        adapter.notifyDataSetChanged();
        adapterMogucePitanje.notifyDataSetChanged();



        boolean postojiDodaj = false;
        for (int i = 0; i < dodajPitanje.size(); i++){
            if (dodajPitanje.get(i).getNaziv().equals("Dodaj pitanje")) postojiDodaj = true;
        }
        if (!postojiDodaj)  dodajPitanje.add(new Pitanje("Dodaj pitanje", R.drawable.plus_icon));

        for (int i=0; i < dodajPitanje.size(); i++){
            pocetnaListaPitanja.add(dodajPitanje.get(i));
        }

        adapter.notifyDataSetChanged();
        adapterMogucePitanje.notifyDataSetChanged();

        lvDodanaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != dodajPitanje.size()-1) {
                    dodajPitanje.get(position).setImage(R.drawable.plus_icon);
                    mogucePitanje.add(mogucePitanje.size(), dodajPitanje.get(position));

                    Pitanje p = dodajPitanje.get(position);

                    dodajPitanje.remove(position);
                    adapter.notifyDataSetChanged();
                    adapterMogucePitanje.notifyDataSetChanged();
                }
                else if (position == dodajPitanje.size()-1) {
                    Intent myIntent = new Intent(DodajKvizAkt.this, DodajPitanjeAkt.class);
                    myIntent.putExtra("listaPitanja", dodajPitanje);
                    DodajKvizAkt.this.startActivityForResult(myIntent, 1);
                }
            }
        });

        lvMogucaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mogucePitanje.size() != 0) {
                    Pitanje novoPitanje = new Pitanje();
                    novoPitanje.setNaziv(mogucePitanje.get(position).getNaziv());
                    novoPitanje.setOdgovori(mogucePitanje.get(position).getOdgovori());
                    novoPitanje.setTacan(mogucePitanje.get(position).getTacan());
                    novoPitanje.setTekstPitanja(mogucePitanje.get(position).getTekstPitanja());
                    Pitanje p = mogucePitanje.get(position);
                    dodajPitanje.add(dodajPitanje.size() - 1, novoPitanje);
                    mogucePitanje.remove(position);

                    adapter.notifyDataSetChanged();
                    adapterMogucePitanje.notifyDataSetChanged();
                }
            }
        });

        spKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == kategorije.size()-1) {
                    Intent myIntent = new Intent(DodajKvizAkt.this, DodajKategorijuAkt.class);
                    DodajKvizAkt.this.startActivityForResult(myIntent, 0);
                    }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnDodajKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String naziv = etNaziv.getText().toString();
                boolean unos = true;
                String prazan = "";
                if (etNaziv.getText().toString().equals(prazan)){
                    etNaziv.setBackgroundColor(Color.RED);
                    unos = false;
                }

                else {
                    if (!KvizoviAkt.uredivanjeKviza){
                        for (int i = 0; i < KvizoviAkt.pomocni.size(); i++) {
                            if (KvizoviAkt.pomocni.get(i).equals(naziv)) {
                                etNaziv.setBackgroundColor(Color.RED);
                                unos = false;
                            }
                      }
                    }

                    if (unos == true){

                        etNaziv.setBackgroundColor(0);
                        boolean konekcija2 = new KvizoviAkt().daLiJeKonektovano(DodajKvizAkt.this);
                        if (!konekcija2) { AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKvizAkt.this);
                            alertDialog.setTitle("Niste spojeni na internet, za unos pitanja spojite se na mrežu!");
                            alertDialog.setIcon(R.drawable.greska);
                            alertDialog.setPositiveButton("OK", null);
                            alertDialog.show();}

                        //popunjavanje mape za dodaj pitanje
                        if (konekcija2){
                        Iterator krozBazuPitanja = KvizoviAkt.pitanjaIzBaze.entrySet().iterator();
                        while(krozBazuPitanja.hasNext()) {
                            Map.Entry pair = (Map.Entry) krozBazuPitanja.next();
                            Pitanje pitanje = (Pitanje) pair.getKey();
                            pitanje.setImage(R.drawable.blue_icon);
                            for (int i = 0; i < dodajPitanje.size(); i++){
                               if (dodajPitanje.get(i) == pitanje){
                                   String proba = (String) pair.getValue();
                                   int k;
                                   for(k = proba.length() - 1; k > 0; k--) {
                                       if (proba.charAt(k) == '/')
                                           break;
                                   }
                                   k++;
                                   proba = proba.substring(k, proba.length());
                                   pitanjaUKvizuIzBaze.put(pitanje, proba);
                               }

                            }

                        }


                        if (kvizoviAkt.uredivanjeKviza){
                        new UrediKviz((output) -> {
                        }).execute("");}
                        else {
                            new DodajKviz().execute();
                        }
                        String slikaId = null;
                        int kate = spKategorije.getSelectedItemPosition();
                        slikaId = kategorije.get(kate).getId();
                        Kategorija kat = new Kategorija(kategorije.get(kate).getNaziv(), slikaId);
                        kviz = new Kviz(etNaziv.getText().toString(), dodajPitanje, kat);
                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("nazivKviza", kviz.getNaziv());
                        returnIntent.putExtra("pitanja", kviz.getPitanja());
                        returnIntent.putExtra("kategorija",kviz.getKategorija().getNaziv());
                        returnIntent.putExtra("idKategorije", kviz.getKategorija().getId());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                }
                }
            }
            }
        });

        for (int i = 0; i < kategorije.size() - 1; i++) {
            pomocniKategorije.add(kategorije.get(i).getNaziv());

        }

        btnImportKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("text/*");
                startActivityForResult(intent, READ_REQUEST_CODE);
            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if (requestCode == 0){
            if (resultCode == RESULT_OK){
                String nazivKategorije = data.getStringExtra("nazivKategorije");
                String idKategorije = data.getStringExtra("idKategroije");
                kategorije.add(kategorije.size()-1, new Kategorija(nazivKategorije, idKategorije));
                KvizoviAkt.kategorije.add(new Kategorija(nazivKategorije, idKategorije));
                adapterKategorija.notifyDataSetChanged();
                pomocniKategorije.add(nazivKategorije);

          }
                if (resultCode == RESULT_CANCELED){

            }
        }

        else if (requestCode == 1){
            if (resultCode == RESULT_OK) {
                String nazivPitanja = data.getStringExtra("nazivPitanja");
                String tekstPitanja = data.getStringExtra("tekstPitanja");
                ArrayList<String> odgovori = (ArrayList<String>) data.getSerializableExtra("odgovori");
                String tacanOdgovor = data.getStringExtra("tacan");
                dodajPitanje.add(dodajPitanje.size() - 1, new Pitanje(nazivPitanja, tekstPitanja, odgovori, tacanOdgovor, R.drawable.blue_icon));
                adapter.notifyDataSetChanged();
            }
            if (resultCode == RESULT_CANCELED){

            }
        }

        else if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            boolean daLiJeKvizOkej = true;
            if (data != null) {
                uri = data.getData();
                String ucitanoIzDatoteke = null;
                try {
                    ucitanoIzDatoteke = readTextFromUri(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String[] redoviDatoteke =  ucitanoIzDatoteke.toString().split("\\n");
                List<String> redovi = Arrays.asList(redoviDatoteke);
                ArrayList<String[]> listaRedova = new ArrayList<>();
                String[] elementiDatoteke = new String[0];
                for (int i = 0; i < redovi.size(); i++) {
                     elementiDatoteke = redovi.get(i).toString().split("\\,");
                     listaRedova.add(elementiDatoteke);
                }
                ArrayList<List<String>> finalnaLista = new ArrayList<>();
                for (int i = 0; i < redovi.size(); i++){
                    List<String> pomocni = Arrays.asList(listaRedova.get(i));
                    finalnaLista.add(pomocni);
                }
                List<String> elementi = Arrays.asList(elementiDatoteke);
                for (int i = 0; i < KvizoviAkt.kvizovi.size(); i++){
                    if (KvizoviAkt.kvizovi.get(i).getNaziv().equals(finalnaLista.get(0).get(0))){
                        daLiJeKvizOkej = false;
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKvizAkt.this);
                        alertDialog.setTitle("Kviz kojeg importujete već postoji!");
                        alertDialog.setIcon(R.drawable.greska);
                        alertDialog.setPositiveButton("OK", null);
                        alertDialog.show();
                    }

                    if (!finalnaLista.get(0).get(2).equals(finalnaLista.size()-1 + "")){
                        if (daLiJeKvizOkej) {
                            daLiJeKvizOkej = false;
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKvizAkt.this);
                            alertDialog.setTitle("Kviz kojeg imporujete ima neispravan broj pitanja!");
                            alertDialog.setIcon(R.drawable.greska);
                            alertDialog.setPositiveButton("OK", null);
                            alertDialog.show();
                        }
                    }

                    //provjera ispravnosti broja odgovora
                    for (int j = 1; j < finalnaLista.size(); j++ ){
                         brojOdgovora = 0;
                          try {
                              brojOdgovora = Integer.valueOf(finalnaLista.get(j).get(1));
                          }
                          catch (NumberFormatException e) {
                              if (daLiJeKvizOkej){
                                daLiJeKvizOkej = false;
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKvizAkt.this);
                                alertDialog.setTitle("Kviz kojeg importujete ima neispravan broj odgovora!");
                                alertDialog.setIcon(R.drawable.greska);
                                  alertDialog.setPositiveButton("OK", null);
                            alertDialog.show();
                        }
                        }
                    }
                    //provjera ispravnosti indexa tacnog odgovora
                    for (int j = 1; j < finalnaLista.size(); j++){
                        int index = 0;
                            try { index = Integer.valueOf(finalnaLista.get(j).get(2));
                                if(brojOdgovora  <= index || index < 0) throw new NumberFormatException();}

                        catch (NumberFormatException e) {
                            if (daLiJeKvizOkej) {
                                daLiJeKvizOkej = false;
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKvizAkt.this);
                                alertDialog.setTitle("Kviz kojeg importujete ima neispravan index tačnog odgovora!");
                                alertDialog.setIcon(R.drawable.greska);
                                alertDialog.setPositiveButton("OK", null);
                                alertDialog.show();
                            }
                        }
                    }
                    //provjera ispravnosti pitanja
                    for (int j = 1; j <= finalnaLista.size() - 2; j++) {
                        for (int k = j + 1; k <= finalnaLista.size() - 1; k++) {
                            if (finalnaLista.get(j).get(0).equals(finalnaLista.get(k).get(0))) {
                                if (daLiJeKvizOkej) {
                                    daLiJeKvizOkej = false;
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKvizAkt.this);
                                    alertDialog.setTitle("Kviz nije ispravan postoje dva pitanja sa istim nazivom!");
                                    alertDialog.setIcon(R.drawable.greska);
                                    alertDialog.setPositiveButton("OK", null);
                                    alertDialog.show();
                                    break;
                                }
                            }
                            if (!daLiJeKvizOkej) break;
                        }
                    }
                    //provjera ispravnosti odgovora
                   for (int j = 1; j < finalnaLista.size(); j++) {
                       for (int k = 3; k < finalnaLista.get(j).size() - 1; k++) {
                           for (int m = k + 1; m < finalnaLista.get(j).size(); m++) {
                               if (finalnaLista.get(j).get(m).equals(finalnaLista.get(j).get(k))) {
                                   if (daLiJeKvizOkej) {
                                       daLiJeKvizOkej = false;
                                       AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKvizAkt.this);
                                       alertDialog.setTitle("Kviz kojeg importujete nije ispravan postoji ponavljanje odgovora");
                                       alertDialog.setIcon(R.drawable.greska);
                                       alertDialog.setPositiveButton("OK", null);
                                       alertDialog.show();
                                       break;
                                   }
                               }
                           }
                           if (!daLiJeKvizOkej) break;
                       }
                       if (!daLiJeKvizOkej) break;
                   }


                    if (daLiJeKvizOkej) {
                        etNaziv.setText(finalnaLista.get(0).get(0));
                        int pozicija = 0;
                        for (int j=0; j < kategorije.size(); j++){
                            if (kategorije.get(j).getNaziv().equals(finalnaLista.get(0).get(1))){
                                pozicija = j;
                            }
                        }
                        if (pozicija == 0){
                            kategorije.add(kategorije.size() - 1, new Kategorija(finalnaLista.get(0).get(1), "13"));
                            spKategorije.setSelection(kategorije.size() - 2);
                            adapterKategorija.notifyDataSetChanged();
                            KvizoviAkt.kategorije.add( new Kategorija(finalnaLista.get(0).get(1), "13"));
                        }
                        else {
                            spKategorije.setSelection(pozicija);
                            adapterKategorija.notifyDataSetChanged();
                        }
                        for (int j = 1; j < finalnaLista.size(); j++) {
                            int pozicijaTacnogOdgovora = Integer.parseInt(finalnaLista.get(j).get(2));
                            ArrayList<String> odgovoriNaPitanje = new ArrayList<>();
                            for (int k = 3; k < finalnaLista.get(j).size(); k++){
                                odgovoriNaPitanje.add(finalnaLista.get(j).get(k));
                            }
                            dodajPitanje.add(dodajPitanje.size() - 1, new Pitanje(finalnaLista.get(j).get(0), finalnaLista.get(j).get(0),odgovoriNaPitanje, finalnaLista.get(j).get(pozicijaTacnogOdgovora + 3)));
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }
    }
    private String readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append('\n');
        }
        inputStream.close();
        stringBuilder.setLength(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }
}
