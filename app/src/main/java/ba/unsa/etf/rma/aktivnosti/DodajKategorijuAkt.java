package ba.unsa.etf.rma.aktivnosti;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.DodajKvizAdapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;

public class DodajKategorijuAkt extends AppCompatActivity implements IconDialog.Callback {

    private EditText etNaziv;
    private EditText etIkona;
    private Button btnDodajIkonu;
    private Button btnDodajKategoriju;
    private Icon[] icon;
    private IconDialog iconDialog = new IconDialog();
    private String ikona;
    public Kategorija novaKategorija = null;
    private String prazan = "";
    boolean ispravan = true;
    KvizoviAkt kvizoviAkt = new KvizoviAkt();


    public class BazaKat extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            InputStream stream = getResources().openRawResource(R.raw.secret);

            try {
                GoogleCredential credential = GoogleCredential.fromStream(stream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credential.refreshToken();
                String TOKEN = credential.getAccessToken();
                Log.d("Sada je token: ", TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");


                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+etNaziv.getText().toString()+"\"},  \"idIkonice\": {\"integerValue\":\"" + Integer.parseInt(etIkona.getText().toString()) +"\"}}}";
                try (OutputStream os = connection.getOutputStream())
                {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    StringBuilder response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    String rezultat = response.toString();
                    JSONObject jo = new JSONObject(rezultat);
                    JSONObject fields = jo.getJSONObject("fields");
                    JSONObject value = fields.getJSONObject("naziv");
                    String naziv = value.getString("stringValue");
                    JSONObject vrijednostIkone = fields.getJSONObject("idIkonice");
                    int idIkonice = vrijednostIkone.getInt(("integerValue"));
                    String name = jo.getString("name");

                    //System.out.println("ID IKONE JEEEEEEEEEEE: " + idIkonice);
                    kvizoviAkt.listaKategorijaIzBaze.put(new Kategorija(naziv, String.valueOf(idIkonice)), name);
                    Log.d("ODGOVOR", response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }



            return null;
        }
    }

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kategoriju_ak);
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        etIkona = (EditText) findViewById(R.id.etIkona);
        btnDodajIkonu = (Button) findViewById(R.id.btnDodajIkonu);
        btnDodajKategoriju = (Button) findViewById(R.id.btnDodajKategoriju);
        etNaziv.setHint("Naziv kategorije");
        etIkona.setHint("Ikona");
        final IconDialog iconDialog = new IconDialog();

        btnDodajIkonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iconDialog.setSelectedIcons(icon);
                iconDialog.show(getSupportFragmentManager(), "icon_dialog");
            }
        });
        btnDodajKategoriju.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                boolean unos = true;

                if(etNaziv.getText().toString().length() == 0){
                    etNaziv.setBackgroundColor(Color.RED);
                    unos = false;
                }
                if (etIkona.getText().toString().length() == 0){
                    etNaziv.setBackgroundColor(Color.RED);
                    unos = false;
                }
                else {
                    Iterator iterator = KvizoviAkt.listaKategorijaIzBaze.entrySet().iterator();
                    while(iterator.hasNext())
                    {
                        Map.Entry pair = (Map.Entry) iterator.next();
                        Kategorija kat = (Kategorija) pair.getKey();
                        if (kat.getNaziv().equals(etNaziv.getText().toString())){
                            etNaziv.setBackgroundColor(Color.RED);
                            unos = false;
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKategorijuAkt.this);
                            alertDialog.setTitle("Unesena kategorija već postoji!");
                            alertDialog.setIcon(R.drawable.greska);
                            alertDialog.setPositiveButton("OK", null);
                            alertDialog.show();
                    }}
                }
                if (unos == true){

                    boolean konekcija2 = new KvizoviAkt().daLiJeKonektovano(DodajKategorijuAkt.this);
                    if (!konekcija2) { AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKategorijuAkt.this);
                        alertDialog.setTitle("Niste spojeni na internet, za unos pitanja spojite se na mrežu!");
                        alertDialog.setIcon(R.drawable.greska);
                        alertDialog.setPositiveButton("OK", null);
                        alertDialog.show();}


                    if (konekcija2) {
                        new BazaKat().execute();
                        btnDodajKategoriju.setEnabled(false);
                        etNaziv.setBackgroundColor(0);
                        Kategorija kategorija = new Kategorija(etNaziv.getText().toString(), etIkona.getText().toString());
                        Intent intent = getIntent();
                        intent.putExtra("nazivKategorije", etNaziv.getText().toString());
                        intent.putExtra("idKategroije", etIkona.getText().toString());
                        setResult(RESULT_OK, intent);
                        finish();

                    }

                }
            } });
  }

    @Override
    public void onIconDialogIconsSelected(Icon[] icons) {
        icon = icons;
        etIkona.setText(String.valueOf(icon[0].getId()));

    }

}
