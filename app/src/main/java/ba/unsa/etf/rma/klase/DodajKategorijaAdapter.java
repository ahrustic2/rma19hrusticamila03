package ba.unsa.etf.rma.klase;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class DodajKategorijaAdapter extends BaseAdapter implements View.OnClickListener{

    private Activity aktivnost;
    private ArrayList lista;
    private static LayoutInflater inflater = null;
    public Resources res;
    Kategorija kategorija = null;
    int brojac = 0;

    public DodajKategorijaAdapter(Activity kvizoviAkt, ArrayList kategorije, Resources resources) {
        aktivnost = kvizoviAkt;
        lista = kategorije;
        res = resources;
        inflater = ( LayoutInflater )aktivnost.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public int getCount() {
        if(lista.size()<=0)
            return 1;
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        public TextView naziv;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        DodajKategorijaAdapter.ViewHolder holder;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.element_pitanje, null);
            holder = new DodajKategorijaAdapter.ViewHolder();
            holder.naziv = (TextView) vi.findViewById(R.id.etNaziv);
            vi.setTag(holder);
        } else {
            holder = (DodajKategorijaAdapter.ViewHolder) vi.getTag();
        }
        if (lista.size() <= 0) {
            holder.naziv.setText("Prazno");
        } else {
            kategorija = (Kategorija) lista.get(position);
            holder.naziv.setText(kategorija.getNaziv());
           }
        return vi;
    }

}
