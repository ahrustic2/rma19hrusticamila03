package ba.unsa.etf.rma.fragmenti;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.DodajKategorijaAdapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaFrag extends Fragment {

    private OnListaFrag mCallback;
    private ListView listaKategorija;
    private ArrayList<Kategorija> kategorije = new ArrayList<>();
    public static DodajKategorijaAdapter kategorijaAdapter;
    private Kategorija svi = new Kategorija("Svi","88");
    public static Kategorija kat = new Kategorija();

    public ListaFrag() {
        // Required empty public constructor
    }

    public interface OnListaFrag {
       public void selektovano(Kategorija kategorija, int pozicija);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lista, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            listaKategorija = (ListView) getView().findViewById(R.id.listaKategorija);
            kategorije = (ArrayList<Kategorija>) getArguments().getSerializable("sveKategorijeZaGrid");
            Resources res = getResources();
            kategorijaAdapter = new DodajKategorijaAdapter(getActivity(), kategorije, res);
            listaKategorija.setAdapter(kategorijaAdapter);

        try {
            mCallback = (OnListaFrag) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString());
        }

        listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mCallback.selektovano(svi, position);
                } else {
                    Kategorija kat = new Kategorija();
                    for (int i = 0; i < kategorije.size(); i++){
                        if (i == position)
                            kat = kategorije.get(i);
                    }
                    kat = KvizoviAkt.kategorije.get(position);
                    mCallback.selektovano(kat, position);
                }
            }
        });
        }


    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

}
