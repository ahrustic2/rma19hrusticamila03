package ba.unsa.etf.rma.fragmenti;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.AsyncResponse;
import ba.unsa.etf.rma.klase.Kviz;

/**
 * A simple {@link Fragment} subclass.
 */
public class RangLista extends Fragment {

    private ListView lvRangLista;
    private String kvizNaziv;
    private String procenat;
    private String igracIme;
    private int pozi = 0;
    private int pozicijaPomocna = 1;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> rangLista = new ArrayList<>();
    private Map<Integer, Map<String, String>> listaIzBaze = new HashMap<>();
    private ArrayList<Pair <Map<Integer, Map<String, String>>, String>> rangListaSaNazivomKvizova = new ArrayList<>();
   private ArrayList<Integer> pozicije = new ArrayList<>();
   private ArrayList<String> zaIspis = new ArrayList<>();
   private ArrayList<Pair<Double, String>> listaSaProcentimaINazivimaIgraca = new ArrayList<>();

    public RangLista() {
        // Required empty public constructor
    }

    public class BazaRangLista extends AsyncTask<String, Void, Void>{

        AsyncResponse delegat = null;

        public BazaRangLista(AsyncResponse delegat){this.delegat = delegat;}

        @Override
        protected void onPostExecute(Void aVoid) {
            delegat.zavrsenProces("");
        }

        @Override
        protected Void doInBackground(String... strings) {
            InputStream inputStream = getResources().openRawResource(R.raw.secret);

            try {
                GoogleCredential credentials = GoogleCredential.fromStream(inputStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                System.out.println("SADA JE TOKEN: " + TOKEN);

                String urlRangLista = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Rangliste?access_token=";
                URL urlObjRangLista = new URL (urlRangLista + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connectionRangLista = (HttpURLConnection) urlObjRangLista.openConnection();
                connectionRangLista.connect();
                InputStream odgovorRangLista = connectionRangLista.getInputStream();
                StringBuilder responseRangLista = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovorRangLista, "utf-8"))
                ){
                    responseRangLista = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        responseRangLista.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR RANG LISTE: ", responseRangLista.toString());
                }
                String rezultat = responseRangLista.toString();
                if(!rezultat.equals("{}")) {
                    JSONObject jsonObject = new JSONObject(rezultat);
                    JSONArray documents = jsonObject.getJSONArray("documents");
                    for (int i = 0; i < documents.length(); i++) {
                        JSONObject doc = documents.getJSONObject(i);
                        JSONObject fields = doc.getJSONObject("fields");
                        JSONObject value = fields.getJSONObject("lista");
                        JSONObject value2 = value.getJSONObject("mapValue");
                        JSONObject fieldsMape = value2.getJSONObject("fields");
                        JSONObject mapa = fieldsMape.getJSONObject("mapa");
                        JSONObject mapa2 = mapa.getJSONObject("mapValue");
                        JSONObject poljaDrugo = mapa2.getJSONObject("fields");
                        JSONObject vrijednostProcenta = poljaDrugo.getJSONObject("procenat");
                        String procenat = vrijednostProcenta.getString("stringValue");
                        JSONObject vrijednostImenaIgraca = poljaDrugo.getJSONObject("imeIgraca");
                        String imeIgraca = vrijednostImenaIgraca.getString("stringValue");
                        JSONObject integerValue = fieldsMape.getJSONObject("pozicija");
                        int pozicijaIgraca = integerValue.getInt("integerValue");
                        JSONObject stringValue = fields.getJSONObject("nazivKviza");
                        String nazivKvizaIzRanga = stringValue.getString("stringValue");
                        Map<String, String> drugaMapa = new HashMap<>();
                        drugaMapa.put(imeIgraca, procenat);
                        listaIzBaze.put(pozicijaIgraca, drugaMapa);
                        rangListaSaNazivomKvizova.add(new Pair <Map<Integer, Map<String, String>>, String>(listaIzBaze, nazivKvizaIzRanga));
                        double procenaDouble = Double.parseDouble(procenat);
                        listaSaProcentimaINazivimaIgraca.add(new Pair<Double, String>(procenaDouble, imeIgraca));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    public class DodajURangListuBaza extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            InputStream inputStream = getResources().openRawResource(R.raw.secret);
            try {
                GoogleCredential credentials = GoogleCredential.fromStream(inputStream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                System.out.println("SADA JE TOKEN: " + TOKEN);

                String urlRangLista = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Rangliste?access_token=";
                URL urlObjRangLista = new URL (urlRangLista + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connectionRangLista = (HttpURLConnection) urlObjRangLista.openConnection();
                connectionRangLista.setDoOutput(true);
                connectionRangLista.setRequestMethod("POST");
                connectionRangLista.setRequestProperty("Content-Type", "application/json");
                connectionRangLista.setRequestProperty("Accept", "application/json");

                String dokument = "{\"fields\" : { \"nazivKviza\" : { \"stringValue\" : \"" + kvizNaziv + "\" }, \"lista\" : { \"mapValue\" : { \"fields\" : { \"pozicija\" : { \"integerValue\" : \"" + pozi + "\" }, \"mapa\" : { \"mapValue\" : { \"fields\" : { \"procenat\" : { \"stringValue\" : \"" + getArguments().getString("procenatTacnih") + "\" }, \"imeIgraca\" : { \"stringValue\" : \"" + getArguments().getString("igrac") + "\" }}}}}}}}}";
                try (OutputStream os = connectionRangLista.getOutputStream())
                {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connectionRangLista.getResponseCode();
                InputStream odgovor = connectionRangLista.getInputStream();
                StringBuilder response = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    void sortiranje(ArrayList<Pair<Double, String>> lista)
    {
        Collections.sort(lista, new Comparator<Pair<Double, String>>() {
            @Override
            public int compare(Pair<Double, String> o1, Pair<Double, String> o2) {
                if (o1.first > o2.first) return -1;
                else if (o1.first == o2.first) return 0;
                else
                return 1;
            }
        });
    }

    public void ispis(){
        listaSaProcentimaINazivimaIgraca.clear();
        ArrayList<Pair <Map<Integer, Map<String, String>>, String>> lista = KvizoviAkt.sqlBazaHelper.rangListaIzSQL();
        for(int i = 0; i < lista.size(); i++){
            String nazivKvizaPom = lista.get(i).second;
            Iterator itera = lista.get(i).first.entrySet().iterator();
            while(itera.hasNext()){
                Map.Entry par1 = (Map.Entry) itera.next();
                Map<String, String> procenatIIgrac = (Map<String, String>) par1.getValue();
                Iterator itera2 = procenatIIgrac.entrySet().iterator();
                while(itera2.hasNext()){
                    Map.Entry par2 = (Map.Entry) itera2.next();
                    double pro = Double.parseDouble((String) par2.getValue());
                    listaSaProcentimaINazivimaIgraca.add(new Pair<Double,String>(pro, (String) par2.getKey()));
                }
            }
        }
        sortiranje(listaSaProcentimaINazivimaIgraca);
        for (int i = 0; i < listaSaProcentimaINazivimaIgraca.size(); i++) {
            boolean pronaden = false;
            String igrac = listaSaProcentimaINazivimaIgraca.get(i).second;
            double pro = listaSaProcentimaINazivimaIgraca.get(i).first;
            for (int j = 0; j < lista.size(); j++) {
                String kviz = lista.get(j).second;
                if (kviz.equals(getArguments().getString("nazivKviza"))) {
                    Map<Integer, Map<String, String>> mapaKviza = (Map<Integer, Map<String, String>>) lista.get(j).first;
                    Iterator iteratorZaKviz = mapaKviza.entrySet().iterator();
                    while (iteratorZaKviz.hasNext()) {
                        Map.Entry par2 = (Map.Entry) iteratorZaKviz.next();
                        int poziIzBaze = (int) par2.getKey();

                        Map<String, String> mapaIgracIProcenat = (Map<String, String>) par2.getValue();
                        Iterator iter = mapaIgracIProcenat.entrySet().iterator();
                        while (iter.hasNext()) {
                            Map.Entry par3 = (Map.Entry) iter.next();
                            String naziv = (String) par3.getKey();
                            if (!pronaden) {
                                if (naziv.equals(igrac)) {
                                    String ispis = pozicijaPomocna + ". " + igrac + " " + pro;
                                    zaIspis.add(ispis);
                                    pozicijaPomocna++;
                                    pronaden = true;
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rang_lista,container,false);
        lvRangLista = (ListView) view.findViewById(R.id.rangLista);
        kvizNaziv = getArguments().getString("nazivKviza");
        igracIme = getArguments().getString("igrac");
        procenat = getArguments().getString("procenatTacnih");
        zaIspis = new ArrayList<>();

        if (KvizoviAkt.konekcija) {
            new BazaRangLista((output) -> {
                int pom = listaIzBaze.size();
                pozi = pom;
                listaIzBaze.clear();
                listaSaProcentimaINazivimaIgraca.clear();
                if (KvizoviAkt.konekcija) {
                    new DodajURangListuBaza().execute();
                }
            }).execute(" ");
        }

        KvizoviAkt.sqlBazaHelper.dodajRanglistuUSQL(getArguments().getString("nazivKviza"), String.valueOf(pozicijaPomocna), getArguments().getString("igrac") , getArguments().getString("procenatTacnih"), KvizoviAkt.database );


        adapter = new ArrayAdapter<String>(getActivity(), R.layout.element_rang, R.id.elementRang, zaIspis) {
            @Override
            public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView prikaz = (TextView) view.findViewById(R.id.elementRang);
                prikaz.setText(zaIspis.get(position));
                return view;
            }
        };
        lvRangLista.setAdapter(adapter);

        ispis();

        return view;}


}

