package ba.unsa.etf.rma.fragmenti;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.OnNmeaMessageListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformacijeFrag extends Fragment {

    private TextView infNazivKviza;
    private TextView infBrojTacnihPitanja;
    private TextView infBrojPreostalihPitanja;
    private TextView infProcenatTacni;
    private Button btnKraj;
    private int brojTacnihOdgovora = 0;
    private  int brojOdigranih = 0;
    private Kraj zavrseno;

    public InformacijeFrag() {
        // Required empty public constructor
    }

    public interface Kraj{
        public void kraj();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_informacije, container, false);
            Bundle bundle = getArguments();
            btnKraj = rootView.findViewById(R.id.btnKraj);

            if(bundle.containsKey("nazivKviza")) {
                infNazivKviza = (TextView) rootView.findViewById(R.id.infNazivKviza);
                infNazivKviza.setText(bundle.getString("nazivKviza"));
                infBrojPreostalihPitanja = (TextView) rootView.findViewById(R.id.infBrojPreostalihPitanja);
                infBrojPreostalihPitanja.setText(bundle.getString("brojPreostalih"));
                infProcenatTacni = (TextView) rootView.findViewById(R.id.infProcenatTacni);
                infProcenatTacni.setText(bundle.getString("procenatTacnih"));
                infBrojTacnihPitanja = (TextView) rootView.findViewById(R.id.infBrojTacnihPitanja);
                infBrojTacnihPitanja.setText(bundle.getString("brojTacnih"));
            }

        try{
            zavrseno = (Kraj) getActivity();
        }
        catch (ClassCastException e){
        }

        btnKraj.setOnClickListener((v) -> {
                    zavrseno.kraj();
                }
        );

        return rootView;
    }



}
