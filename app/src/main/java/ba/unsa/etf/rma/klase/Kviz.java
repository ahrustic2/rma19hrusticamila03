package ba.unsa.etf.rma.klase;

import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;

public class Kviz implements Parcelable {
    private String naziv;
    private ArrayList<Pitanje> pitanja;
    private Kategorija kategorija;

    public Kviz() {
        naziv = "";
        pitanja = null;
        kategorija = new Kategorija();
    }

    public Kviz(String naziv, ArrayList<Pitanje> pitanja, Kategorija kategorija) {
        this.naziv = naziv;
        this.pitanja = pitanja;
        this.kategorija = kategorija;
    }


    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Pitanje> getPitanja() {
        return pitanja;
    }

    public void setPitanja(ArrayList<Pitanje> pitanja) {
        this.pitanja = pitanja;
    }

    public Kategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(Kategorija kategorija) {
        this.kategorija = kategorija;
    }

    void dodajPitanje(Pitanje pitanje){
        pitanja.add(pitanje);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeSerializable(kategorija);
        dest.writeArray(new ArrayList[]{pitanja});
    }
}
