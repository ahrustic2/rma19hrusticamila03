package ba.unsa.etf.rma.klase;

import java.io.Serializable;

public class Kategorija implements Serializable {
    private String naziv;
    private String id;

    public Kategorija() {
        naziv = "Svi";
        id = "88";
    }

    public Kategorija(String naziv, String id) {
        if (naziv != "")
        this.naziv = naziv;
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
