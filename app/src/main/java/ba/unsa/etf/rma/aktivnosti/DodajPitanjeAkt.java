package ba.unsa.etf.rma.aktivnosti;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.TacanOdgovor;

public class DodajPitanjeAkt extends AppCompatActivity {

    private EditText etNaziv;
    private EditText etOdgovor;
    private ListView lvOdgovori;
    private Button btnDodajOdgovor;
    private Button btnDodajTacan;
    private Button btnDodajPitanje;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> odgovori = new ArrayList<>();
    private int brojTacnih = 0;
    public static TacanOdgovor tacanOdgovor;
    private String tOdgovor;
    private boolean tacanOdgovorBool = false;
    private int pozicijaTacnogODgovora;
    private DodajKvizAkt dodajKvizAkt;
    private Pitanje pitanje;
    private KvizoviAkt kvizoviAkt;


    public class BazaPitanje extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            InputStream stream = getResources().openRawResource(R.raw.secret);

            try {
                GoogleCredential credential = GoogleCredential.fromStream(stream).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credential.refreshToken();
                String TOKEN = credential.getAccessToken();
                Log.d("Sada je token: ", TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/rmaspirala-13/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");

                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+etNaziv.getText().toString()+"\"} , \"odgovori\" : { \"arrayValue\" : { \"values\" : [";
                for(int i = 0; i < odgovori.size(); i++){
                    if(i != odgovori.size() - 1) {
                        dokument += "{\"stringValue\" : \"" + odgovori.get(i) + "\"},";
                    }
                    else{
                        dokument += "{\"stringValue\" : \"" + odgovori.get(i) + "\"}";
                    }
                }
                dokument+= "]}} , \"indexTacnog\": {\"integerValue\":\"" + pozicijaTacnogODgovora +"\"}}}";

                try (OutputStream os = connection.getOutputStream())
                {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    StringBuilder response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }

                    String rezultat = response.toString();
                    JSONObject jo = new JSONObject(rezultat);
                    String name = jo.getString("name");
                    dodajKvizAkt.pitanjaUKvizuIzBaze.put(pitanje, name);
                    KvizoviAkt.pitanjaIzBaze.put(pitanje, name);
                    Log.d("ODGOVOR", response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_pitanje_akt);
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        etOdgovor = (EditText) findViewById(R.id.etOdgovor);
        lvOdgovori = (ListView) findViewById(R.id.lvOdgovori);
        btnDodajOdgovor = (Button) findViewById(R.id.btnDodajOdgovor);
        btnDodajTacan = (Button) findViewById(R.id.btnDodajTacan);
        btnDodajPitanje = (Button) findViewById(R.id.btnDodajPitanje);
        etNaziv.setHint("Naziv pitanja - INPUT");
        etOdgovor.setHint("Odgovor");
        Resources resources = getResources();
        adapter = new ArrayAdapter<String>(this, R.layout.element, R.id.etNaziv, odgovori){
            @Override
            public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                String s = odgovori.get(position);
                ImageView icon = (ImageView) view.findViewById(R.id.icon);
                TextView naziv = (TextView) view.findViewById(R.id.etNaziv);
                naziv.setText(s);
                if (position == odgovori.size() - 1) {
                    if (!tacanOdgovorBool) {
                        naziv.setBackgroundColor(0);
                        tacanOdgovorBool = false;
                    }
                    else if (tacanOdgovorBool){
                            naziv.setBackgroundColor(Color.GREEN);
                            tacanOdgovorBool = false;
                    }
                }
                return view;
            }
        };
        lvOdgovori.setAdapter(adapter);


        btnDodajOdgovor.setOnClickListener((v)->{
            boolean jedinstven = true;
            tacanOdgovorBool = false;
            String odg = etOdgovor.getText().toString();
            String prazan = "";
            if (odg.equals(prazan)) {
                jedinstven = false;
                etOdgovor.setBackgroundColor(Color.RED);
            }
            else {
                if (odgovori.size() == 0)  {
                    odgovori.add(odg);
                    etOdgovor.setText("");
                    etOdgovor.setBackgroundColor(0);
                    adapter.notifyDataSetChanged();
                }
                else {
                    for (int i = 0; i < odgovori.size(); i++){
                    if (odgovori.get(i).equals(odg)) jedinstven = false;
                    }
                    if (jedinstven == true) {
                        odgovori.add(odg);
                        etOdgovor.setText("");
                        etOdgovor.setBackgroundColor(0);
                        adapter.notifyDataSetChanged();
                 }
                 else  etOdgovor.setBackgroundColor(Color.RED);
                }
            }
        });

        btnDodajTacan.setOnClickListener((v)->{
            boolean jedinstven = true;
            String odg = etOdgovor.getText().toString();
            String prazan = "";
            if (brojTacnih == 0) {
                if (odg.equals(prazan)) {
                    jedinstven = false;
                    etOdgovor.setBackgroundColor(Color.RED);
                } else {
                    if (odgovori.size() == 0) {
                        odgovori.add(odg);
                        etOdgovor.setText("");
                        etOdgovor.setBackgroundColor(0);
                        tacanOdgovor = new TacanOdgovor(odg);
                        tOdgovor = odg;
                        brojTacnih++;
                        tacanOdgovorBool = true;
                        pozicijaTacnogODgovora = odgovori.size()-1;
                        adapter.notifyDataSetChanged();
                    } else {
                        for (int i = 0; i < odgovori.size(); i++) {
                            if (odgovori.get(i).equals(odg)) jedinstven = false;
                        }
                        if (jedinstven == true) {
                            odgovori.add(odg);
                            etOdgovor.setText("");
                            brojTacnih++;
                            tOdgovor = odg;
                            tacanOdgovorBool = true;
                            etOdgovor.setBackgroundColor(0);
                            pozicijaTacnogODgovora = odgovori.size()-1;
                            adapter.notifyDataSetChanged();
                        } else etOdgovor.setBackgroundColor(Color.RED);
                    }
                }
            }
            else btnDodajTacan.setEnabled(false);
        });

        adapter.notifyDataSetChanged();
        lvOdgovori.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(odgovori.get(position).equals(tOdgovor)) {
                        brojTacnih--;
                        tacanOdgovorBool = false;
                        tOdgovor = "";
                        btnDodajTacan.setEnabled(true);
                        adapter.notifyDataSetChanged();
                    }
                    odgovori.remove(position);
                    adapter.notifyDataSetChanged();

            }
        });

        btnDodajPitanje.setOnClickListener((v)->{
            boolean unos = true;
            String naziv = etNaziv.getText().toString();
            String prazan = "";
           if (naziv.equals(prazan)) {
               etNaziv.setBackgroundColor(Color.RED);
               unos = false;
           }
           else {
               ArrayList<Pitanje> postojecaPitanja = (ArrayList<Pitanje>)getIntent().getSerializableExtra("listaPitanja");
               for (int i = 0; i < postojecaPitanja.size(); i++){
                   if(postojecaPitanja.get(i).getNaziv().equals(naziv))
                   {etNaziv.setBackgroundColor(Color.RED);
                   unos = false;}
               }
           }

           if (odgovori.size() == 0) {
               etOdgovor.setBackgroundColor(Color.RED);
               unos = false;
           }
           if (brojTacnih == 0) {
               etOdgovor.setBackgroundColor(Color.RED);
               unos = false;
           }

            Iterator it = kvizoviAkt.pitanjaIzBaze.entrySet().iterator();
            while(it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                Pitanje pitanje = (Pitanje) pair.getKey();
                if (pitanje.getNaziv().equals(etNaziv.getText().toString())) {
                    unos = false;
                    etNaziv.setBackgroundColor(Color.RED);
                    break;
                }

            }

           if (unos == true) {
               boolean konekcija2 = new KvizoviAkt().daLiJeKonektovano(DodajPitanjeAkt.this);
               if (!konekcija2) {
                   AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajPitanjeAkt.this);
                   alertDialog.setTitle("Niste spojeni na internet, za unos pitanja spojite se na mrežu!");
                   alertDialog.setIcon(R.drawable.greska);
                   alertDialog.setPositiveButton("OK", null);
                   alertDialog.show();
               }
               if (konekcija2) {
                   etNaziv.setBackgroundColor(0);
                   new BazaPitanje().execute();
                   Intent i = getIntent();
                   pitanje = new Pitanje(etNaziv.getText().toString(), etNaziv.getText().toString(), odgovori, tOdgovor);
                   i.putExtra("tacan", tOdgovor);
                   i.putExtra("tekstPitanja", etNaziv.getText().toString());
                   i.putExtra("odgovori", odgovori);
                   i.putExtra("nazivPitanja", etNaziv.getText().toString());
                   setResult(RESULT_OK, i);
                   finish();
               }
           }
        });
    }
}

