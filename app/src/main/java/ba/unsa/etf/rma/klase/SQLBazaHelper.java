package ba.unsa.etf.rma.klase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.fragmenti.RangLista;

public class SQLBazaHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "rmaspirala-13.db";
    public static final int DATABASE_VERSION = 13;


    // Kreiranje tabele za ranglista
    private static final String DATABASE_TABLE_RANGLISTA = "Ranglista";
    private static final String ID_RANGLISTA = "_id";
    private static final String NAZIV_KVIZA_RANGLISTA = "nazivKviza";
    private static final String POZICIJA_U_RANGLISTI = "pozicija";
    private static final String IGRAC_RANGLISTA = "nazivIgraca";
    private static final String PROCENAT_TACNIH_ODGOVORA_RANGLISTA = "procenatTacnih";

    private static final String DATABASE_CREATE_RANGLISTA = "CREATE TABLE " +
            DATABASE_TABLE_RANGLISTA + " ("
            + ID_RANGLISTA + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAZIV_KVIZA_RANGLISTA + " TEXT NOT NULL, "
            + POZICIJA_U_RANGLISTI + " TEXT NOT NULL, "
            + IGRAC_RANGLISTA + " TEXT NOT NULL, "
            + PROCENAT_TACNIH_ODGOVORA_RANGLISTA + " TEXT );";


    //Kreiranje tabele za pitanje
    private static final String DATABASE_TABLE_PIT = "Pitanja";
    private static final String ID_PITANJE = "Id";
    private static final String NAZIV_PITANJE = "naziv";
    private static final String PITANJE_INDEX_TACNOG_ODGOVORA = "indexTacnog";
    private static final String ODGOVORI_PITANJE = "odgovori";

    private static final String DATABASE_CREATE_PITANJE = "CREATE TABLE " +
            DATABASE_TABLE_PIT + " ("
            + ID_PITANJE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAZIV_PITANJE + " TEXT NOT NULL, "
            + PITANJE_INDEX_TACNOG_ODGOVORA + " INTEGER, "
            + ODGOVORI_PITANJE + " TEXT NOT NULL);";


    //Kreiranje tabele za kviz
    private static final String DATABASE_TABLE_KVIZ = "Kviz";
    private static final String ID_KVIZA = "_id";
    private static final String NAZIV_KVIZA = "naziv";
    private static final String ID_KATEGORIJE_KVIZA = "idKategorije";
    private static final String PITANJA_KVIZA = "pitanja";

    private static final String DATABASE_CREATE_KVIZ = "CREATE TABLE " +
            DATABASE_TABLE_KVIZ + " ("
            + ID_KVIZA + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAZIV_KVIZA + " TEXT NOT NULL, "
            + ID_KATEGORIJE_KVIZA + " TEXT NOT NULL, "
            + PITANJA_KVIZA + " TEXT NOT NULL);";


    //Kreiranje tabele za kategorije
    private static final String DATABASE_TABLE_KAT = "Kategorija";
    private static final String ID_KATEGORIJE = "_id";
    private static final String NAZIV_KATEGORIJE = "naziv";
    private static final String ID_IKONE_KATEGORIJE = "idIkonice";

    private static final String DATABASE_CREATE_KATEGORIJA = "CREATE TABLE " +
            DATABASE_TABLE_KAT + " ("
            + ID_KATEGORIJE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAZIV_KATEGORIJE + " TEXT NOT NULL, "
            + ID_IKONE_KATEGORIJE + " INTEGER NOT NULL);";



    public SQLBazaHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_PITANJE);
        db.execSQL(DATABASE_CREATE_KATEGORIJA);
        db.execSQL(DATABASE_CREATE_KVIZ);
        db.execSQL(DATABASE_CREATE_RANGLISTA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_PIT);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KAT);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KVIZ);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_RANGLISTA);
        onCreate(db);
    }

    public static String convertArrayToStri(ArrayList<String> strArray) {
        String rezultat = "";

        for (int i = 0; i < strArray.size(); i++) {
            rezultat += strArray.get(i);
            if (i == strArray.size() - 1) {

            } else {
                rezultat += ",";
            }
        }
        return rezultat;
    }



    public boolean dodajPitanjeUSQL(Pitanje pitanje, SQLiteDatabase db) {

        int pozicijaTacnog = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAZIV_PITANJE, pitanje.getNaziv());
        for (int i = 0; i < pitanje.getOdgovori().size(); i++){
            if (pitanje.getOdgovori().get(i).equals(pitanje.getTacan())) pozicijaTacnog = i;
        }
        contentValues.put(PITANJE_INDEX_TACNOG_ODGOVORA, pozicijaTacnog);
        contentValues.put(ODGOVORI_PITANJE, convertArrayToStri(pitanje.getOdgovori()));

        long rezultat = db.insert(DATABASE_TABLE_PIT, null, contentValues);
        if (rezultat == -1) return false;
        else
            return true;
    }


    public boolean dodajKategorijuUSQL(Kategorija kategorija, SQLiteDatabase database){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID_IKONE_KATEGORIJE, kategorija.getId());
        contentValues.put(NAZIV_KATEGORIJE, kategorija.getNaziv());

        long rezultat = database.insert(DATABASE_TABLE_KAT, null, contentValues);
        if (rezultat == -1)
            return false;
        else
            return true;
    }

    public boolean dodajKvizUSQL (Kviz kviz, SQLiteDatabase database){
        ContentValues contentValues = new ContentValues();
        ArrayList<String> nazivPitanja = new ArrayList<>();
        for (int i = 0; i < kviz.getPitanja().size(); i++){
            nazivPitanja.add(kviz.getPitanja().get(i).getNaziv());
        }
        String pitanja = convertArrayToStri(nazivPitanja);
        contentValues.put(NAZIV_KVIZA, kviz.getNaziv());
        contentValues.put(PITANJA_KVIZA, pitanja);
        contentValues.put(ID_KATEGORIJE_KVIZA, kviz.getKategorija().getNaziv());
        long rezultat = database.insert(DATABASE_TABLE_KVIZ, null, contentValues);
        if (rezultat == -1)
            return false;
        else
            return true;
    }

    public boolean dodajRanglistuUSQL(String nazivKviza, String pozicija, String igrac, String procenat, SQLiteDatabase database){
        ContentValues contentValues = new ContentValues();

        contentValues.put(NAZIV_KVIZA_RANGLISTA, nazivKviza);
        contentValues.put(POZICIJA_U_RANGLISTI, pozicija);
        contentValues.put(IGRAC_RANGLISTA, igrac);
        contentValues.put(PROCENAT_TACNIH_ODGOVORA_RANGLISTA, procenat);

        long rezultat = database.insert(DATABASE_TABLE_RANGLISTA, null, contentValues);
        if (rezultat == -1)
            return false;
        else
            return true;
    }


    public ArrayList<Pair<Map<Integer, Map<String, String>>, String>> rangListaIzSQL() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c;
        ArrayList<Pair<Map<Integer, Map<String, String>>, String>> rangLista = new ArrayList<>();
        c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_RANGLISTA, null);
        int id = c.getColumnIndexOrThrow(ID_RANGLISTA);
        int kviz= c.getColumnIndexOrThrow(NAZIV_KVIZA_RANGLISTA);
        int igrac = c.getColumnIndexOrThrow(IGRAC_RANGLISTA);
        int procenat = c.getColumnIndexOrThrow(PROCENAT_TACNIH_ODGOVORA_RANGLISTA);
        int pozicija = c.getColumnIndexOrThrow(POZICIJA_U_RANGLISTI);
        while (c.moveToNext()) {
            Map<String, String> unutrasnjaMapa = new HashMap<>();
            unutrasnjaMapa.put(c.getString(igrac), c.getString(procenat));
            Map<Integer, Map<String, String>> trenutnaMapa = new HashMap<>();
            trenutnaMapa.put(Integer.parseInt(c.getString(pozicija)), unutrasnjaMapa);
            Pair par = new Pair(trenutnaMapa, c.getString(kviz));
            rangLista.add(par);
        }
        c.close();
        return rangLista;
    }



    public ArrayList<Pitanje> pitanjaIZSQL(SQLiteDatabase db) {
        Cursor cursor;
        ArrayList<Pitanje> pitanja = new ArrayList<>();

        cursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_PIT, null);

        int id = cursor.getColumnIndexOrThrow(ID_PITANJE);
        int naziv = cursor.getColumnIndexOrThrow(NAZIV_PITANJE);
        int index = cursor.getColumnIndexOrThrow(PITANJE_INDEX_TACNOG_ODGOVORA);
        int odgovori = cursor.getColumnIndexOrThrow(ODGOVORI_PITANJE);

        while (cursor.moveToNext()) {
            Pitanje p = new Pitanje();
            p.setNaziv(cursor.getString(naziv));
            p.setTekstPitanja(cursor.getString(naziv));
            p.setOdgovori(convertStringToArray(cursor.getString(odgovori)));
            p.setTacan(p.getOdgovori().get(cursor.getInt(index)));
            pitanja.add(p);

        }

        cursor.close();
        return pitanja;
    }

    public ArrayList<Kviz> kvizoviIZSQL(SQLiteDatabase db) {
        Cursor cursor;
        ArrayList<Kviz> kvizici = new ArrayList<>();

        cursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_KVIZ, null);


        int id = cursor.getColumnIndexOrThrow(ID_KVIZA);
        int naziv = cursor.getColumnIndexOrThrow(NAZIV_KVIZA);
        int kat = cursor.getColumnIndexOrThrow(ID_KATEGORIJE_KVIZA);
        int pitanja = cursor.getColumnIndexOrThrow(PITANJA_KVIZA);

        while (cursor.moveToNext()) {
            Kviz pom = new Kviz();
            pom.setNaziv(cursor.getString(naziv));
            Kategorija kate = new Kategorija();
           for (int i = 0; i < KvizoviAkt.kategorije.size(); i++){
                if (KvizoviAkt.kategorije.get(i).getNaziv().equals(cursor.getString(kat))) {
                    kate = KvizoviAkt.kategorije.get(i);
                    break;
                }
            }
            pom.setKategorija(kate);
            ArrayList<String> listaNazivaPitanja = new ArrayList<>();

            listaNazivaPitanja.addAll(Arrays.asList(cursor.getString(pitanja).split(",")));
            ArrayList<Pitanje> pitanjaZaKviz = new ArrayList<Pitanje>();

            for (int i = 0; i < KvizoviAkt.pitanjaIzSQL.size(); i++) {
                for (int j = 0; j < listaNazivaPitanja.size(); j++) {
                    if (KvizoviAkt.pitanjaIzSQL.get(i).getNaziv().equals(listaNazivaPitanja.get(j))) {

                        pitanjaZaKviz.add((KvizoviAkt.pitanjaIzSQL.get(i)));
                        break;
                    }
                }
            }
            pom.setPitanja(pitanjaZaKviz);

            kvizici.add(pom);
        }
        cursor.close();
        return kvizici;
    }


    private ArrayList<String> convertStringToArray(String string) {
        ArrayList<String> odgovori = new ArrayList<>();
        odgovori.addAll(Arrays.asList(string.split(",")));

        return odgovori;
    }

    public ArrayList<Kategorija> katgorijeIzSQL(SQLiteDatabase db) {
        Cursor cursor;
        ArrayList<Kategorija> kategorije = new ArrayList<>();

        cursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_KAT, null);

        int id = cursor.getColumnIndexOrThrow(ID_KATEGORIJE);
        int naziv = cursor.getColumnIndexOrThrow(NAZIV_KATEGORIJE);
        int ikona = cursor.getColumnIndexOrThrow(ID_IKONE_KATEGORIJE);

        while (cursor.moveToNext()) {
            Kategorija kategorija = new Kategorija();
            kategorija.setNaziv(cursor.getString(naziv));
            kategorija.setId(Integer.toString(cursor.getInt(ikona)));
            kategorije.add(kategorija);
        }

        cursor.close();
        return kategorije;
    }

    public void obrisiSQLBazu(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ DATABASE_TABLE_PIT);
        db.execSQL("DELETE FROM "+ DATABASE_TABLE_KAT);
        db.execSQL("DELETE FROM "+ DATABASE_TABLE_KVIZ);
    }
    public void obrisiRangListu(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ DATABASE_TABLE_RANGLISTA);
    }

}
